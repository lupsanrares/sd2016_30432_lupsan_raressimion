﻿using AutoMapper;
using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.Repositories
{
    public class UserRepository : EntityRepository<AspNetUser, UserDTO>, IUserRepository
    {
        IContext _context;
        public UserRepository(IContext context)
            : base(context)
        {
            _context = context;
            _dbset = _context.Set<AspNetUser>();
        }

        public override IEnumerable<UserDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<AspNetUser>, IEnumerable<UserDTO>>(_context.AspNetUsers).ToList();
        }

        public UserDTO GetById(string Id)
        {
            return Mapper.Map<UserDTO>(_dbset.FirstOrDefault(x => x.Id.Equals(Id)));

        }
        public UserDTO GetByUsername(string Id)
        {
            return Mapper.Map<UserDTO>(_dbset.FirstOrDefault(x => x.UserName.Equals(Id)));

        }

        public override void Create(UserDTO user)
        {
            if (user == null)
            {
                return;
            }
            if (GetById(user.Id) == null)
            {

                var c = Mapper.Map<AspNetUser>(user);
                _dbset.Add(c);
                _context.Entry(c).State = EntityState.Added;
                _context.SaveChanges();
            }
        }
        public override void Update(UserDTO user)
        {
            if (user == null)
            {
                return;
            }
            var userDTO = _dbset.FirstOrDefault(x => x.Id.Equals(user.Id));
            if (userDTO != null)
            {
                userDTO.UserName = user.Email;
                userDTO.Email = user.Email;
                userDTO.PhoneNumber = user.PhoneNumber;
                _context.Entry(userDTO).CurrentValues.SetValues(userDTO);
                _context.Entry(userDTO).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
        public override void Delete(UserDTO user)
        {
            if (user == null)
            {
                return;
            }
            var userDTO = _dbset.FirstOrDefault(x => x.Id.Equals(user.Id));
            if (userDTO != null)
            {
                _dbset.Remove(userDTO);
                _context.Entry(userDTO).State = EntityState.Deleted;
                _context.SaveChanges();
            }
        }
    }
}
