﻿using AutoMapper;
using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.Repositories
{
    public class PacientRepository : EntityRepository<Pacient, PacientDTO>, IPacientRepository
    {
        IContext _context;
        public PacientRepository(IContext context)
            : base(context)
        {
            _context = context;
            _dbset = _context.Set<Pacient>();
        }

        public override IEnumerable<PacientDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<Pacient>, IEnumerable<PacientDTO>>(_context.Pacients).ToList();
        }

        public PacientDTO GetById(string pnc)
        {
            return Mapper.Map<PacientDTO>(_dbset.FirstOrDefault(x => x.PNC.Equals(pnc)));

        }

        public override void Create(PacientDTO Pacient)
        {
            if (Pacient == null)
            {
                return;
            }
            if (GetById(Pacient.PNC) == null)
            {

                var c = Mapper.Map<Pacient>(Pacient);
                _dbset.Add(c);
                _context.Entry(c).State = EntityState.Added;
                _context.SaveChanges();
            }
        }
        public override void Update(PacientDTO Pacient)
        {
            if (Pacient == null)
            {
                return;
            }
            var PacientDTO = _dbset.FirstOrDefault(x => x.PNC.Equals(Pacient.PNC));
            if (PacientDTO != null)
            {
                var c = Mapper.Map<Pacient>(Pacient);
                _context.Entry(PacientDTO).CurrentValues.SetValues(c);
                _context.Entry(PacientDTO).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
        public override void Delete(PacientDTO Pacient)
        {
            if (Pacient == null)
            {
                return;
            }
            var PacientDTO = _dbset.FirstOrDefault(x => x.PNC.Equals(Pacient.PNC));
            if (PacientDTO != null)
            {
                _dbset.Remove(PacientDTO);
                _context.Entry(PacientDTO).State = EntityState.Deleted;
                _context.SaveChanges();
            }
        }
    }
}
