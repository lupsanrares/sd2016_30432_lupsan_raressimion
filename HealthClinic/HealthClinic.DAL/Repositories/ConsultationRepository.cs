﻿using AutoMapper;
using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.Repositories
{
    public class ConsultationRepository : EntityRepository<Consultation, ConsultationDTO>, IConsultationRepository
    {
        public ConsultationRepository(IContext context)
            : base(context)
        {
            _context = context;
            _dbset = _context.Set<Consultation>();
        }
        public override IEnumerable<ConsultationDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<Consultation>, IEnumerable<ConsultationDTO>>(_context.Consultations).ToList();
        }

        public override void Create(ConsultationDTO consultation)
        {
            var c = Mapper.Map<Consultation>(consultation);
            _dbset.Add(c);
            _context.SaveChanges();
        }
        public IEnumerable<ConsultationDTO> GetByDateAndDoctor(DateTime date, String id)
        {
            var list=_dbset.Where(t => t.Date.Equals(date) && t.DoctorId.Equals(id)).AsEnumerable();
            return Mapper.Map<IEnumerable<Consultation>, IEnumerable<ConsultationDTO>>(list);
        }
        public ConsultationDTO GetById(int Id)
        {
            return Mapper.Map<ConsultationDTO>(_dbset.Where(t => t.Id == Id).FirstOrDefault());
        }
        public override void Update(ConsultationDTO consultationDTO)
        {
            if (consultationDTO == null)
            {
                return;
            }
            var consultation = _dbset.FirstOrDefault(x => x.Id==consultationDTO.Id);
            if (consultation != null)
            {
                var c = Mapper.Map<Consultation>(consultationDTO);
                _context.Entry(consultation).CurrentValues.SetValues(c);
                _context.Entry(consultation).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
    }
}
