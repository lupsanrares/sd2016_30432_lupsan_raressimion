﻿using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.Repositories
{
    public interface IPacientRepository : IEntityRepository<Pacient, PacientDTO>
    {
        PacientDTO GetById(string Id);
    }
}
