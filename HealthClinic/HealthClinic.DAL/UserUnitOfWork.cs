﻿using HealthClinic.DAL.Repositories;
using HealthClinic.DAL.UoW;
using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL
{
    public class UserUnitOfWork : IUserUnitOfWork
    {
        #region Fields
        private readonly healthclinicEntities _context;
        private IUserRepository _userRepository;
        #endregion

        #region Constructors
        public UserUnitOfWork(IUserRepository repository)
        {
            _context = new healthclinicEntities();
            _userRepository = repository;
        }
        #endregion

        #region IUnitOfWork Members
        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(_context)); }
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _userRepository.GetAll();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _userRepository = null;
            _context.Dispose();
        }
        #endregion


        public void Create(UserDTO entity)
        {
            _userRepository.Create(entity);
        }

        public void Delete(UserDTO entity)
        {
            _userRepository.Delete(entity);
        }

        public void Update(UserDTO entity)
        {
            _userRepository.Update(entity);
        }

        public UserDTO GetById(string Id)
        {
            return _userRepository.GetById(Id);
        }
        public UserDTO GetByUsername(string Id)
        {
            return _userRepository.GetByUsername(Id);
        }
    }

}
