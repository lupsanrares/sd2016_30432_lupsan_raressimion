﻿using HealthClinic.DAL.Repositories;
using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.UoW
{
    public class ConsultationUnitOfWork:IConsultationUnitOfWork
    {
         #region Fields
        private readonly healthclinicEntities _context;
        private IPacientRepository _pacientRepository;
        private IConsultationRepository _consultationRepository;
        #endregion

        #region Constructors
        public ConsultationUnitOfWork(IPacientRepository repository, IConsultationRepository consultationRepo)
        {
            _context = new healthclinicEntities();
            _pacientRepository= repository;
            _consultationRepository = consultationRepo;
        }
        #endregion

        #region IUnitOfWork Members
        public IPacientRepository PacientRepository
        {
            get { return _pacientRepository ?? (_pacientRepository = new PacientRepository(_context)); }
        }
        public IConsultationRepository ConsultationRepository
        {
            get { return _consultationRepository ?? (_consultationRepository = new ConsultationRepository(_context)); }
        }
        public IEnumerable<PacientDTO> GetAll()
        {
            return _pacientRepository.GetAll();
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _pacientRepository = null;
            _context.Dispose();
        }
        #endregion

        public void Create(ConsultationDTO entity)
        {
            _consultationRepository.Create(entity);
        }

        public void Delete(ConsultationDTO entity)
        {
            throw new NotImplementedException();
        }

        IEnumerable<ConsultationDTO> IUnitOfWork<ConsultationDTO>.GetAll()
        {
            return _consultationRepository.GetAll();
        }

        public void Update(ConsultationDTO entity)
        {
            _consultationRepository.Update(entity);
        }


        public List<HealthClinic.Model.Enums.Hours> GetConsultationWithAvailableHours(ConsultationDTO consultation)
        {
           List<HealthClinic.Model.Enums.Hours> toReturn=new List<HealthClinic.Model.Enums.Hours>(); 
           var x= _consultationRepository.GetByDateAndDoctor(consultation.Date, consultation.DoctorId).ToList();
           foreach(HealthClinic.Model.Enums.Hours hour in Enum.GetValues(typeof(HealthClinic.Model.Enums.Hours)))
           {
               var z = x.Where(t => t.Hour.Equals(hour.ToString())).ToList();
               if(z.Count==0)
               {
                   toReturn.Add(hour);
               }
           }
           return toReturn;
        }
        public ConsultationDTO GetById(int Id)
        {
            return _consultationRepository.GetById(Id);
        }
    }
}
