﻿using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.UoW
{
    public interface IUserUnitOfWork : IUnitOfWork<UserDTO>
    {
        UserDTO GetById(string Id);
        UserDTO GetByUsername(string Id);
    }
}
