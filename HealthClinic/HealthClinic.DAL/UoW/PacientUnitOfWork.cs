﻿using HealthClinic.DAL.Repositories;
using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.DAL.UoW
{
    public class PacientUnitOfWork:IPacientUnitOfWork
    {
         #region Fields
        private readonly healthclinicEntities _context;
        private IPacientRepository _pacientRepository;
        private IConsultationRepository _consultationRepository;
        #endregion

        #region Constructors
        public PacientUnitOfWork(IPacientRepository repository,IConsultationRepository consultationRepo)
        {
            _context = new healthclinicEntities();
            _pacientRepository= repository;
            _consultationRepository = consultationRepo;
        }
        #endregion

        #region IUnitOfWork Members
        public IPacientRepository PacientRepository
        {
            get { return _pacientRepository ?? (_pacientRepository = new PacientRepository(_context)); }
        }
        public IConsultationRepository ConsultationRepository
        {
            get { return _consultationRepository ?? (_consultationRepository = new ConsultationRepository(_context)); }
        }
        public IEnumerable<PacientDTO> GetAll()
        {
            return _pacientRepository.GetAll();
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _pacientRepository = null;
            _context.Dispose();
        }
        #endregion


        public void Create(PacientDTO entity)
        {
            _pacientRepository.Create(entity);
        }

        public void Delete(PacientDTO entity)
        {
           
            var consultations=_consultationRepository.GetAll().ToList();
            consultations = consultations.Where(t => t.PacientPNC.Equals(entity.PNC)).ToList();
            foreach (var consultation in consultations)
            {
                _consultationRepository.Delete(consultation);
            }
            _pacientRepository.Delete(entity);
        }

        public void Update(PacientDTO entity)
        {   
            _pacientRepository.Update(entity);
        }

        public PacientDTO GetById(string pnc)
        {
            return _pacientRepository.GetById(pnc);
        }
    }
}
