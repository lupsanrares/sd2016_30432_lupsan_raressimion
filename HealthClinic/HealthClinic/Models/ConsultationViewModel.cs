﻿using HealthClinic.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthClinic.Models
{
    public class ConsultationViewModel
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public string PacientPNC { get; set; }
        public UserDTO Doctor { get; set; }
        public string DoctorId { get; set; }
        public List<HealthClinic.Model.Enums.Hours> AvailableHours { get; set; }
        public List<UserDTO> Doctors { get; set; }
        public String Hour { get; set; }
        public String Description { get; set; }
    }
}