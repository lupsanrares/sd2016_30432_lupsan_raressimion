﻿using Autofac;
using HealthClinic.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace HealthClinic.App_Start
{
    public class DependencyInjection:Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType(typeof(healthclinicEntities)).As(typeof(IContext)).InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("HealthClinic.DAL"))

                  .Where(t => t.Name.EndsWith("UnitOfWork"))

                  .AsImplementedInterfaces()

                  .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("HealthClinic.DAL"))

                   .Where(t => t.Name.EndsWith("Repository"))

                   .AsImplementedInterfaces()

                   .InstancePerLifetimeScope();
        }
    }
}