﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using AutoMapper;
using HealthClinic.Model;
using HealthClinic.Models;
using HealthClinic.DAL.UoW;

namespace HealthClinic.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private IUserUnitOfWork _UserUnitOfWork;

        public AdminController(IUserUnitOfWork UserUoW)
        {
            _UserUnitOfWork = UserUoW;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return this._roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set { this._roleManager = value; }
        }
        public ActionResult Index()
        {
            List<EmployeeViewModel> toReturn = new List<EmployeeViewModel>();
            var employees = Mapper.Map<IEnumerable<UserDTO>, IEnumerable<EmployeeViewModel>>(_UserUnitOfWork.GetAll()).ToList();
            foreach(var employee in employees)
            {
                if(UserManager.IsInRole(employee.Id,"Secretary"))
                {
                    employee.Type = RegisterViewModel.AccountType.Secretary;
                    toReturn.Add(employee);
                }
                else
                {
                    if(UserManager.IsInRole(employee.Id,"Doctor"))
                    {
                        employee.Type = RegisterViewModel.AccountType.Doctor;
                        toReturn.Add(employee);
                    }
                }
                
            }

            return View(toReturn);
        }
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber,Name=model.Name };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    var userInserted = await UserManager.FindByEmailAsync(model.Email);
                   await UserManager.AddToRoleAsync(userInserted.Id, model.Type.ToString());
                }
            }
            return RedirectToAction("Index", "Admin");
        }

        public ActionResult Edit(String Id)
        {
            var employee = Mapper.Map<UserDTO, EmployeeViewModel>(_UserUnitOfWork.GetById(Id));
            return View(employee);
        }

        // POST: Client/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(EmployeeViewModel employee)
        {
            try
            {
                    _UserUnitOfWork.Update(new UserDTO() { Id = employee.Id, Email = employee.Email, PhoneNumber = employee.PhoneNumber,Name=employee.Name });
                return RedirectToAction("Index");
            }
            catch
            {
                return View(employee);
            }
        }
        public ActionResult Delete(String Id)
        {
            var employee = Mapper.Map<UserDTO, EmployeeViewModel>(_UserUnitOfWork.GetById(Id));
            return View(employee);
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(EmployeeViewModel employee)
        {
            try
            {

                _UserUnitOfWork.Delete(new UserDTO() { Id = employee.Id, Email = employee.Email, PhoneNumber = employee.PhoneNumber });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}