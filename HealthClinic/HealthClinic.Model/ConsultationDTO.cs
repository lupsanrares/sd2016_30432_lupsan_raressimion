﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthClinic.Model
{
    public class ConsultationDTO
    {

        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public string Hour { get; set; }
        [Display(Name="Pacient")]
        public string PacientPNC { get; set; }
        [Display(Name = "Doctor")]
        public string DoctorId { get; set; }
        public string Description { get; set; }
    }
}
