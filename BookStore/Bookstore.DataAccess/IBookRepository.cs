﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAll();
        Book GetById(int id);
        int Insert(Book book);
        void Update(Book book);
        void Delete(int id);
        IEnumerable<Book> SearchByTitle(string searchText);
        IEnumerable<Book> SearchByAuthor(List<Author> authors);
        IEnumerable<Book> SearchByGenre(List<Genre> genres);
    }
}
