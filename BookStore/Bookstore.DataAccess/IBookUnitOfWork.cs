﻿using BookStore.Models;
using BookStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public interface IBookUnitOfWork
    {
        IEnumerable<BookViewModel> GetAll();
        BookViewModel GetById(int id);
        int Create(BookViewModel book);
        void Update(BookViewModel book);
        void Sell(BookViewModel book);
        void Delete(int id);
        IEnumerable<BookViewModel> SearchByTitle(string searchText);
        IEnumerable<BookViewModel> SearchByAuthor(string searchText);
        IEnumerable<BookViewModel> SearchByGenre(string searchText);
        String GenerateReport(String type);
    }
}
