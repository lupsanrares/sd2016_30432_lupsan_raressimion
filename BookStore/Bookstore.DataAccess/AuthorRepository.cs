﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Bookstore.DataAccess
{
    public class AuthorRepository:IAuthorRepository
    {
        private  XDocument m_doc;
        private  String path = HttpContext.Current.Server.MapPath("~/App_Data/authors.xml");
        private  int AuthId = 0;
        public  Author GetById(int id)
        {
            m_doc = XDocument.Load(path);

            if (m_doc != null && m_doc.Element("Authors") != null)
            {
                return m_doc.Element("Authors").Elements("Author").Select(e => new Author()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).Where(e => e.Id == id).FirstOrDefault();
            }
            return null;
        }
        public  Author GetByName(String name)
        {
            m_doc = XDocument.Load(path);

            if (m_doc != null && m_doc.Element("Authors") != null)
            {
                return m_doc.Element("Authors").Elements("Author").Select(e => new Author()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).Where(e => e.Name.Equals(name)).FirstOrDefault();
            }
            return null;
        }
        public  IEnumerable<Author> SearchByName(String name)
        {
            List<Author> authors = new List<Author>();
            m_doc = XDocument.Load(path);

            if (m_doc != null && m_doc.Element("Authors") != null)
            {
                authors.AddRange(m_doc.Element("Authors").Elements("Author").Select(e => new Author()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).Where(e => e.Name.ToLower().Contains(name.ToLower())).ToList());
            }
            return authors;
        }
        public int Insert(String authorName)
        {
            m_doc = XDocument.Load(path);
            List<Author> authors = new List<Author>();
            if (m_doc != null && m_doc.Element("Authors") != null)
            {
                authors.AddRange(m_doc.Element("Authors").Elements("Author").Select(e => new Author()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).ToList());
            }
            Author auth = authors.Where(e => e.Name.Equals(authorName)).FirstOrDefault();
            if (auth == null)
            {
                int id = authors.OrderByDescending(e => e.Id).FirstOrDefault().Id;
                id++;
                authors.Add(new Author()
                {
                    Id = id,
                    Name = authorName
                }
                    );
                var xml = new XElement("Authors", authors.Select(x => new XElement("Author", new XElement("Id", x.Id), new XElement("Name", x.Name))));
                xml.Save(path);
                return id;
            }
            return -1;
        }
    }
}
