﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public class UserUnitOfWork:IUserUnitOfWork
    {
        IUserRepository _UserRepository;
        IBookRepository _BookRepository;
        public UserUnitOfWork(IUserRepository _UserRepo,IBookRepository _BookRepo)
        {
            _UserRepository = _UserRepo;
            _BookRepository = _BookRepo;
        }
        public User Login(String username, String password)
        {
            var user=_UserRepository.CheckUser(username, password);
            if(user!=null)
            {
                user.Roles = _UserRepository.GetRoles(user);
            }
            return user;
        }
        public IEnumerable<User> GetAll()
        {
            return _UserRepository.GetAll();
        }
        public List<string> GetRoles(User user)
        {
            return _UserRepository.GetRoles(user).ToList();
        }
        public void Create(User user)
        {

            _UserRepository.Create(user);
            _UserRepository.AddToRole(user, "Regular_User");
        }
        public User FindById(string userId)
        {
            return _UserRepository.FindById(userId);
        }
        public void Delete(string Id)
        {
            _UserRepository.Delete(Id);
        }
        public void Update(User user)
        {
            _UserRepository.Update(user);
        }
       
    }
}
