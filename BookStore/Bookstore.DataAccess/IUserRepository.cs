﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public interface IUserRepository
    {
        User CheckUser(string username, string password);
        IList<string> GetRoles(User user);
        IEnumerable<User> GetAll();
        void Create(User user);
        void AddToRole(User user, string roleName);
        User FindById(string userId);
        void Delete(String id);
        void Update(User user);
    }
}
