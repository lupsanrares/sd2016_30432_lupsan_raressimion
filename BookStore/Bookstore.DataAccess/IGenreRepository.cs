﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public interface IGenreRepository
    {
        Genre GetById(int id);
        Genre GetByName(String name);
        IEnumerable<Genre> SearchByName(String name);
        int Insert(String genreName);

    }
}
