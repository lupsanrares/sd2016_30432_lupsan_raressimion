﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Bookstore.DataAccess
{
    public class GenreRepository:IGenreRepository
    {
        private XDocument m_doc;

        private String path = HttpContext.Current.Server.MapPath("~/App_Data/genres.xml");
        public Genre GetById(int id)
        {
            m_doc = XDocument.Load(path);

            if (m_doc != null && m_doc.Element("Genres") != null)
            {
                return m_doc.Element("Genres").Elements("Genre").Select(e => new Genre()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).Where(e => e.Id == id).FirstOrDefault();
            }
            return null;
        }

        public Genre GetByName(String name)
        {
            m_doc = XDocument.Load(path);

            if (m_doc != null && m_doc.Element("Genres") != null)
            {
                return m_doc.Element("Genres").Elements("Genre").Select(e => new Genre()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).Where(e => e.Name.Equals(name)).FirstOrDefault();
            }
            return null;
        }
        public IEnumerable<Genre> SearchByName(String name)
        {
            List<Genre> genres = new List<Genre>();
            m_doc = XDocument.Load(path);

            if (m_doc != null && m_doc.Element("Genres") != null)
            {
                genres.AddRange(m_doc.Element("Genres").Elements("Genre").Select(e => new Genre()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).Where(e => e.Name.ToLower().Contains(name.ToLower())).ToList());
            }
            return genres;
        }
        public int Insert(String genreName)
        {
            m_doc = XDocument.Load(path);
            List<Author> genres = new List<Author>();
            if (m_doc != null && m_doc.Element("Genres") != null)
            {
                genres.AddRange(m_doc.Element("Genres").Elements("Genre").Select(e => new Author()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Name = e.Element("Name").Value,

                }).ToList());
            }
            Author genre = genres.Where(e => e.Name.Equals(genreName)).FirstOrDefault();
            if (genre == null)
            {
                int id = genres.OrderByDescending(e => e.Id).FirstOrDefault().Id;
                id++;
                genres.Add(new Author()
                {
                    Id = id,
                    Name = genreName
                }
                    );
                var xml = new XElement("Genres", genres.Select(x => new XElement("Genre", new XElement("Id", x.Id), new XElement("Name", x.Name))));
                xml.Save(path);
                return id;
            }
            else
                return genre.Id;
        }
    }
}
