﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Bookstore.DataAccess
{
    public class UserRepository : IUserRepository
    {
        protected XDocument m_doc;
        protected string credentialsXmlFile = HttpContext.Current.Server.MapPath("~/App_Data/users.xml");
        protected string rolesXmlFile = HttpContext.Current.Server.MapPath("~/App_Data/roles.xml");
        protected string userRolesXmlFile = HttpContext.Current.Server.MapPath("~/App_Data/userroles.xml");

        public User FindById(string userId)
        {

            m_doc = XDocument.Load(credentialsXmlFile);
            List<User> users = new List<User>();
            if (m_doc != null && m_doc.Element("Users") != null && m_doc.Element("Users").Elements("User").Count() > 0)
            {
                users.AddRange(m_doc.Element("Users").Elements("User").Select(e => new User()
                {
                    Id = (string)e.Element("Id"),
                    PasswordHash = (string)e.Element("PasswordHash"),
                    UserName = (string)e.Element("UserName"),
                    Name = (string)e.Element("Name"),
                    PhoneNumber = (string)e.Element("PhoneNumber"),
                    Address = (string)e.Element("Address")
                }).ToList());
            }
            User user1 = null;
            if (users.Count > 0)
            {
                user1 = users.Where(e => e.Id.Equals(userId)).FirstOrDefault();
            }

            return user1;
        }

        public User CheckUser(string username, string password)
        {

            m_doc = XDocument.Load(credentialsXmlFile);
            List<User> users = new List<User>();
            if (m_doc != null && m_doc.Element("Users") != null && m_doc.Element("Users").Elements("User").Count() > 0)
            {
                users.AddRange(m_doc.Element("Users").Elements("User").Select(e => new User()
                {
                    Id = (string)e.Element("Id"),
                    PasswordHash = (string)e.Element("PasswordHash"),
                    UserName = (string)e.Element("UserName"),
                    Name = (string)e.Element("Name"),
                    PhoneNumber = (string)e.Element("PhoneNumber"),
                    Address = (string)e.Element("Address")
                }).ToList());
            }
            User user1 = null;
            if (users.Count > 0)
            {
                user1 = users.Where(e => e.UserName.Equals(username) && e.PasswordHash.Equals(password)).FirstOrDefault();
            }

            return user1;
        }

        public User FindByName(string userName)
        {
            return FindById(userName);

        }
        public IEnumerable<User> GetAll()
        {
            m_doc = XDocument.Load(credentialsXmlFile);
            List<User> users = new List<User>();
            if (m_doc != null && m_doc.Element("Users") != null && m_doc.Element("Users").Elements("User").Count() > 0)
            {
                users.AddRange(m_doc.Element("Users").Elements("User").Select(e => new User()
                {
                    Id = (string)e.Element("Id"),
                    PasswordHash = (string)e.Element("PasswordHash"),
                    UserName = (string)e.Element("UserName"),
                    Name = (string)e.Element("Name"),
                    PhoneNumber = (string)e.Element("PhoneNumber"),
                    Address = (string)e.Element("Address")
                }).ToList());
            }
            return users;
        }
        public void Create(User user)
        {
            m_doc = XDocument.Load(credentialsXmlFile);
            List<User> users = new List<User>();
            if (m_doc != null && m_doc.Element("Users") != null && m_doc.Element("Users").Elements("User").Count() > 0)
            {
                users.AddRange(m_doc.Element("Users").Elements("User").Select(e => new User()
                {
                    Id = (string)e.Element("Id"),
                    PasswordHash = (string)e.Element("PasswordHash"),
                    UserName = (string)e.Element("UserName"),
                    Name = (string)e.Element("Name"),
                    PhoneNumber = (string)e.Element("PhoneNumber"),
                    Address = (string)e.Element("Address")
                }).ToList());
            }
            List<User> usersT = users.Where(e => e.UserName.Equals(user.UserName)).ToList();
            if (usersT.Count == 0)
            {
                users.Add(user);
                var xml = new XElement("Users", users.Select(x => new XElement("User", new XElement("Id", x.Id), new XElement("UserName", x.UserName), new XElement("PasswordHash", x.PasswordHash), new XElement("Name", x.Name), new XElement("Address", x.Address), new XElement("PhoneNumber", x.PhoneNumber))));
                xml.Save(credentialsXmlFile);
                AddToRole(user, "Regular_User");
            }
        }
        public void AddToRole(User user, string roleName)
        {
            m_doc = XDocument.Load(rolesXmlFile);
            int roleId = -1;
            if (m_doc != null && m_doc.Element("Roles") != null && m_doc.Element("Roles").Elements("Role").Count() > 0)
            {
                roleId = m_doc.Element("Roles").Elements("Role").Where(e => ((string)e.Element("Name")).Equals(roleName)).Select(e => (int)e.Element("Id")).FirstOrDefault();
            }
            m_doc = XDocument.Load(userRolesXmlFile);
            List<Tuple<string, int>> roles = new List<Tuple<string, int>>();
            if (m_doc != null && m_doc.Element("UserRoles") != null && m_doc.Element("UserRoles").Elements("UserRole").Count() > 0)
            {
                roles.AddRange(m_doc.Element("UserRoles").Elements("UserRole").Select(e => new Tuple<string, int>((string)e.Element("UserId"), (int)e.Element("RoleId"))).ToList());
            }
            roles.Add(new Tuple<string, int>(user.Id, roleId));
            var xml = new XElement("UserRoles", roles.Select(x => new XElement("UserRole", new XElement("UserId", x.Item1), new XElement("RoleId", x.Item2))));
            xml.Save(userRolesXmlFile);
        }

        public IList<string> GetRoles(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var u = FindById(user.Id);
            if (u == null)
                throw new ArgumentException("IdentityUser does not correspond to a User entity.", "user");
            m_doc = XDocument.Load(userRolesXmlFile);
            List<int> roles = new List<int>();
            if (m_doc != null && m_doc.Element("UserRoles") != null && m_doc.Element("UserRoles").Elements("UserRole").Count() > 0)
            {
                roles.AddRange(m_doc.Element("UserRoles").Elements("UserRole").Where(e => ((string)e.Element("UserId")).Equals(user.Id)).Select(e => (int)e.Element("RoleId")).ToList());
            }
            m_doc = XDocument.Load(rolesXmlFile);
            List<string> roleNames = new List<string>();
            if (m_doc != null && m_doc.Element("Roles") != null && m_doc.Element("Roles").Elements("Role").Count() > 0)
            {
                roleNames.AddRange(m_doc.Element("Roles").Elements("Role").Where(e => roles.Contains((int)e.Element("Id"))).Select(e => (string)e.Element("Name")).ToList());
            }
            return roleNames;
        }
        public void Delete(String id)
        {
            m_doc = XDocument.Load(credentialsXmlFile);
            List<User> users = new List<User>();
            if (m_doc != null && m_doc.Element("Users") != null && m_doc.Element("Users").Elements("User").Count() > 0)
            {
                users.AddRange(m_doc.Element("Users").Elements("User").Select(e => new User()
                {
                    Id = (string)e.Element("Id"),
                    PasswordHash = (string)e.Element("PasswordHash"),
                    UserName = (string)e.Element("UserName"),
                    Name = (string)e.Element("Name"),
                    PhoneNumber = (string)e.Element("PhoneNumber"),
                    Address = (string)e.Element("Address")
                }).ToList());
            }
            List<User> usersT = users.Where(e => !e.Id.Equals(id)).ToList();

            if (usersT.Count > 0)
            {
                var xml = new XElement("Users", usersT.Select(x => new XElement("User", new XElement("Id", x.Id), new XElement("UserName", x.UserName), new XElement("PasswordHash", x.PasswordHash), new XElement("Name", x.Name), new XElement("Address", x.Address), new XElement("PhoneNumber", x.PhoneNumber))));
                xml.Save(credentialsXmlFile);
            }
            else
            {
                var xml = new XElement("Users", "");

                xml.Save(credentialsXmlFile);
            }
            DeleteFromRole(id, "Regular_User");
        }
        public void DeleteFromRole(String id, String roleName)
        {

            m_doc = XDocument.Load(rolesXmlFile);
            int roleId = -1;
            if (m_doc != null && m_doc.Element("Roles") != null && m_doc.Element("Roles").Elements("Role").Count() > 0)
            {
                roleId = m_doc.Element("Roles").Elements("Role").Where(e => ((string)e.Element("Name")).Equals(roleName)).Select(e => (int)e.Element("Id")).FirstOrDefault();
            }
            m_doc = XDocument.Load(userRolesXmlFile);
            List<Tuple<string, int>> roles = new List<Tuple<string, int>>();
            if (m_doc != null && m_doc.Element("UserRoles") != null && m_doc.Element("UserRoles").Elements("UserRole").Count() > 0)
            {
                roles.AddRange(m_doc.Element("UserRoles").Elements("UserRole").Select(e => new Tuple<string, int>((string)e.Element("UserId"), (int)e.Element("RoleId"))).ToList());
            }
            roles = roles.Where(t => !t.Item1.Equals(id)).ToList();
            if (roles.Count > 0)
            {
                var xml = new XElement("UserRoles", roles.Select(x => new XElement("UserRole", new XElement("UserId", x.Item1), new XElement("RoleId", x.Item2))));
                xml.Save(userRolesXmlFile);
            }
            else
            {
                var xml = new XElement("UserRoles", "");
                xml.Save(userRolesXmlFile);
            }
        }
        public void Update(User user)
        {
            m_doc = XDocument.Load(credentialsXmlFile);
            List<User> users = new List<User>();
            if (m_doc != null && m_doc.Element("Users") != null && m_doc.Element("Users").Elements("User").Count() > 0)
            {
                users.AddRange(m_doc.Element("Users").Elements("User").Select(e => new User()
                {
                    Id = (string)e.Element("Id"),
                    PasswordHash = (string)e.Element("PasswordHash"),
                    UserName = (string)e.Element("UserName"),
                    Name = (string)e.Element("Name"),
                    PhoneNumber = (string)e.Element("PhoneNumber"),
                    Address = (string)e.Element("Address")
                }).ToList());
            }
            User usersT = users.Where(e => e.UserName.Equals(user.UserName)).FirstOrDefault();
            if (usersT!=null)
            {
                usersT.Name = user.Name;
                usersT.PhoneNumber = user.PhoneNumber;
                usersT.Address = user.Address;
                var xml = new XElement("Users", users.Select(x => new XElement("User", new XElement("Id", x.Id), new XElement("UserName", x.UserName), new XElement("PasswordHash", x.PasswordHash), new XElement("Name", x.Name), new XElement("Address", x.Address), new XElement("PhoneNumber", x.PhoneNumber))));
                xml.Save(credentialsXmlFile);
                AddToRole(user, "Regular_User");
            }
        }
        
    }
}