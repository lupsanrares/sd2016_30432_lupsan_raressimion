﻿using BookStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace Bookstore.DataAccess
{
    public class BookRepository:IBookRepository
    {
        private XDocument m_doc;
        private String path = HttpContext.Current.Server.MapPath("~/App_Data/books.xml");
        public int Insert(Book createdBook)
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                {
                    Id = (int)e.Element("Id"),
                    Title = (string)e.Element("Title"),
                    AuthorId = (int)e.Element("AuthorId"),
                    GenreId = (int)e.Element("GenreId"),
                    Quantity = (int)e.Element("Quantity"),
                    Price = (double)e.Element("Price")
                }).ToList());
            }
            Book book = null;
            int bookId = 1;
            if (books.Count > 0)
            {
                book = books.Where(e => e.Title.Equals(createdBook.Title) && e.AuthorId == createdBook.AuthorId).FirstOrDefault();
                bookId = books.OrderByDescending(e => e.Id).FirstOrDefault().Id;
            }
            if (book == null)
            {
                createdBook.Id = bookId + 1;
                books.Add(createdBook);
                var xml = new XElement("Books", books.Select(x => new XElement("Book", new XElement("Id", x.Id), new XElement("Title", x.Title), new XElement("AuthorId", x.AuthorId), new XElement("GenreId", x.GenreId), new XElement("Quantity", x.Quantity), new XElement("Price", x.Price))));
                xml.Save(path);
                return createdBook.Id;
            }

            return book.Id;
        }
        public Book GetById(int id)
        {
            m_doc = XDocument.Load(path);
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                return m_doc.Element("Books").Elements("Book").Select(e => new Book()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Title = e.Element("Title").Value,
                    AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                    GenreId = Int32.Parse(e.Element("GenreId").Value),
                    Quantity = Int32.Parse(e.Element("Quantity").Value),
                    Price = Double.Parse(e.Element("Price").Value)
                }).Where(e => e.Id == id).FirstOrDefault();
            }
            return null;
        }
        public IEnumerable<Book> GetAll()
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Title = e.Element("Title").Value,
                    AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                    GenreId = Int32.Parse(e.Element("GenreId").Value),
                    Quantity = Int32.Parse(e.Element("Quantity").Value),
                    Price = Double.Parse(e.Element("Price").Value)
                }).ToList());
            }
            return books;
        }

        public void Update(Book b)
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Title = e.Element("Title").Value,
                    AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                    GenreId = Int32.Parse(e.Element("GenreId").Value),
                    Quantity = Int32.Parse(e.Element("Quantity").Value),
                    Price = Double.Parse(e.Element("Price").Value)
                }).ToList());
            }
            Book updatedBook = books.Where(e => e.Id == b.Id).FirstOrDefault();
            updatedBook.Title = b.Title;
            updatedBook.AuthorId = b.AuthorId;
            updatedBook.GenreId = b.GenreId;
            updatedBook.Price = b.Price;
            updatedBook.Quantity = b.Quantity;
            var xml = new XElement("Books", books.Select(x => new XElement("Book", new XElement("Id", x.Id), new XElement("Title", x.Title), new XElement("AuthorId", x.AuthorId), new XElement("GenreId", x.GenreId), new XElement("Quantity", x.Quantity), new XElement("Price", x.Price))));
            xml.Save(path);
        }
        public void Delete(int id)
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Title = e.Element("Title").Value,
                    AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                    GenreId = Int32.Parse(e.Element("GenreId").Value),
                    Quantity = Int32.Parse(e.Element("Quantity").Value),
                    Price = Double.Parse(e.Element("Price").Value)
                }).ToList());
            }
            books = books.Where(e => e.Id != id).ToList();
            if (books.Count > 0)
            {
                var xml = new XElement("Books", books.Select(x => new XElement("Book", new XElement("Id", x.Id), new XElement("Title", x.Title), new XElement("AuthorId", x.AuthorId), new XElement("GenreId", x.GenreId), new XElement("Quantity", x.Quantity), new XElement("Price", x.Price))));
                xml.Save(path);
            }
            else
            {
                var xml = new XElement("Books", "");
                xml.Save(path);
            }

        }

        public IEnumerable<Book> SearchByTitle(string searchText)
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                {
                    Id = Int32.Parse(e.Element("Id").Value),
                    Title = e.Element("Title").Value,
                    AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                    GenreId = Int32.Parse(e.Element("GenreId").Value),
                    Quantity = Int32.Parse(e.Element("Quantity").Value),
                    Price = Double.Parse(e.Element("Price").Value)
                }).Where(e => e.Title.ToLower().Contains(searchText.ToLower())).ToList());
            }
            return books;
        }

        public IEnumerable<Book> SearchByAuthor(List<Author> authors)
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                for (int i = 0; i < authors.Count; i++)
                {
                    Author t = authors.ElementAt(i);
                    books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                    {
                        Id = Int32.Parse(e.Element("Id").Value),
                        Title = e.Element("Title").Value,
                        AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                        GenreId = Int32.Parse(e.Element("GenreId").Value),
                        Quantity = Int32.Parse(e.Element("Quantity").Value),
                        Price = Double.Parse(e.Element("Price").Value)
                    }).Where(e => e.AuthorId == t.Id).ToList());
                }
            }
            return books;
        }

        public IEnumerable<Book> SearchByGenre(List<Genre> genres)
        {
            m_doc = XDocument.Load(path);
            List<Book> books = new List<Book>();
            if (m_doc != null && m_doc.Element("Books") != null && m_doc.Element("Books").Elements("Book").Count() > 0)
            {
                for (int i = 0; i < genres.Count; i++)
                {
                    Genre t = genres.ElementAt(i);
                    books.AddRange(m_doc.Element("Books").Elements("Book").Select(e => new Book()
                    {
                        Id = Int32.Parse(e.Element("Id").Value),
                        Title = e.Element("Title").Value,
                        AuthorId = Int32.Parse(e.Element("AuthorId").Value),
                        GenreId = Int32.Parse(e.Element("GenreId").Value),
                        Quantity = Int32.Parse(e.Element("Quantity").Value),
                        Price = Double.Parse(e.Element("Price").Value)
                    }).Where(e => e.GenreId == t.Id).ToList());
                }
            }
            return books;
        }
       
    }
}
