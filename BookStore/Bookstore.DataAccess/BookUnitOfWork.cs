﻿using BookStore.Models;
using BookStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DataAccess
{
    public class BookUnitOfWork:IBookUnitOfWork
    {
        IBookRepository _BookRepository;
        IAuthorRepository _AuthorRepository;
        IGenreRepository _GenreRepository;
        public BookUnitOfWork(IBookRepository bookRepo,IAuthorRepository authorRepo,IGenreRepository genreRepo)
        {
            _BookRepository = bookRepo;
            _AuthorRepository = authorRepo;
            _GenreRepository = genreRepo;
        }
        public int Create(BookViewModel book)
        {
            Author author = _AuthorRepository.GetByName(book.Author);
            int authorId, genreId;
            if (author == null)
            {
                authorId = _AuthorRepository.Insert(book.Author);
            }
            else
            {
                authorId = author.Id;
            }
            Genre genre = _GenreRepository.GetByName(book.Genre);
            if (genre == null)
            {
                genreId = _GenreRepository.Insert(book.Genre);
            }
            else
            {
                genreId = genre.Id;
            }
            Book CreatedBook = new Book()
            {
                Title = book.Title,
                AuthorId = authorId,
                GenreId = genreId,
                Price = book.Price,
                Quantity = book.Quantity
            };
            return _BookRepository.Insert(CreatedBook);

        }


        public IEnumerable<BookViewModel> GetAll()
        {
            var books=_BookRepository.GetAll();
            List<BookViewModel> toReturn = books.Select(t => new BookViewModel()
            {
                Id = t.Id,
                Title = t.Title,
                Author = _AuthorRepository.GetById(t.AuthorId).Name,
                Genre = _GenreRepository.GetById(t.GenreId).Name,
                Price = t.Price,
                Quantity = t.Quantity
            }).ToList();
            return toReturn;
        }

        public BookViewModel GetById(int id)
        {
            Book book = _BookRepository.GetById(id);
            BookViewModel bvm = new BookViewModel()
            {
                Id = book.Id,
                Title = book.Title,
                Author = _AuthorRepository.GetById(book.AuthorId).Name,
                Genre = _GenreRepository.GetById(book.GenreId).Name,
                Price = book.Price,
                Quantity = book.Quantity
            };
            return bvm;
        }

        public void Update(BookViewModel book)
        {
            Author author = _AuthorRepository.GetByName(book.Author);
            int authorId, genreId;
            if (author == null)
            {
                authorId = _AuthorRepository.Insert(book.Author);
            }
            else
            {
                authorId = author.Id;
            }
            Genre genre = _GenreRepository.GetByName(book.Genre);
            if (genre == null)
            {
                genreId = _GenreRepository.Insert(book.Genre);
            }
            else
            {
                genreId = genre.Id;
            }
            Book CreatedBook = new Book()
            {
                Id=book.Id,
                Title = book.Title,
                AuthorId = authorId,
                GenreId = genreId,
                Price = book.Price,
                Quantity = book.Quantity
            };
            _BookRepository.Update(CreatedBook);

        }
        public void Sell(BookViewModel book)
        {
            var soldBook=_BookRepository.GetById(book.Id);
            soldBook.Quantity = soldBook.Quantity - book.Quantity;
            _BookRepository.Update(soldBook);

        }
        public void Delete(int id)
        {
            _BookRepository.Delete(id);
        }

        public IEnumerable<BookViewModel> SearchByTitle(string searchText)
        {
            var books = _BookRepository.SearchByTitle(searchText);
            List<BookViewModel> toReturn = books.Select(t => new BookViewModel()
            {
                Id = t.Id,
                Title = t.Title,
                Author = _AuthorRepository.GetById(t.AuthorId).Name,
                Genre = _GenreRepository.GetById(t.GenreId).Name,
                Price = t.Price,
                Quantity = t.Quantity
            }).ToList();
            return toReturn;
        }

        public IEnumerable<BookViewModel> SearchByAuthor(string searchText)
        {     
            List<Author> authors = new List<Author>();
            authors = _AuthorRepository.SearchByName(searchText).ToList();
            var books= _BookRepository.SearchByAuthor(authors);
            List<BookViewModel> toReturn = books.Select(t => new BookViewModel()
            {
                Id = t.Id,
                Title = t.Title,
                Author = _AuthorRepository.GetById(t.AuthorId).Name,
                Genre = _GenreRepository.GetById(t.GenreId).Name,
                Price = t.Price,
                Quantity = t.Quantity
            }).ToList();
            return toReturn;
        }

        public IEnumerable<BookViewModel> SearchByGenre(string searchText)
        {
            List<Genre> genres= _GenreRepository.SearchByName(searchText).ToList();
            var books = _BookRepository.SearchByGenre(genres);
            List<BookViewModel> toReturn = books.Select(t => new BookViewModel()
            {
                Id = t.Id,
                Title = t.Title,
                Author = _AuthorRepository.GetById(t.AuthorId).Name,
                Genre = _GenreRepository.GetById(t.GenreId).Name,
                Price = t.Price,
                Quantity = t.Quantity
            }).ToList();
            return toReturn;
        }
        public String GenerateReport(String type)
        {
            var books = _BookRepository.GetAll().Where(t => t.Quantity == 0).ToList();
            Report[] reports = new Report[2];
            reports[0] = new PDFReport(books);
            reports[1] = new CSVReport(books);
            if(type.Equals("csv"))
                 return reports[1].FileName;
            else
                return reports[0].FileName;
        }
    }
}
