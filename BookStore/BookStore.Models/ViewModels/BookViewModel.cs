﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookStore.Models.ViewModels
{
    public class BookViewModel
    {

        public int Id { get; set; }
         [Required]
        public String Title { get; set; }
         [Required]
        public String Author { get; set; }
         [Required]
        public String Genre { get; set; }
         [Required]
        [RegularExpression("([0-9]+)",ErrorMessage="Value must be integer")]
        [Range(0.0, Int32.MaxValue, ErrorMessage = "Invalid quantity specified")]
        public int Quantity { get; set; }
         [Required]
        [Range(0.0, Double.MaxValue, ErrorMessage = "Invalid Price specified")]
        public Double Price { get; set; }
       

    }
}