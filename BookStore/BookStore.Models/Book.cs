﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace BookStore.Models
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]
    [IgnoreFirst()]
    public class Book
    {
      
        public int Id { get; set; }
        public String Title { get; set; }
        public int AuthorId { get; set; }
        public int GenreId { get; set; }

        public int Quantity { get; set; }
        public Double Price { get; set; }
    }
}