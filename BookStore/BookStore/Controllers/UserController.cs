﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using BookStore.Models;
using BookStore.App_Start;
using PagedList;
using BookStore.Models.ViewModels;
using Bookstore.DataAccess;
namespace BookStore.Controllers
{
    [CustomAuthorization(IdentityRoles = "Admin")]
    public class UserController : Controller
    {
        IUserUnitOfWork _UserUnitOfWork;
        public UserController(IUserUnitOfWork userUoW)
        {
            _UserUnitOfWork = userUoW;
        }

        // GET: Book
        public ActionResult Index(int page = 1, int pageSize = 10)
        {
            var users = _UserUnitOfWork.GetAll();

            return View(users.ToPagedList(page, 10));
        }
        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            try
            {
                user.Id = user.UserName;
                var u = _UserUnitOfWork.FindById(user.Id);
                if (u == null)
                    _UserUnitOfWork.Create(user);
                else
                    return View(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Edit/5
        public ActionResult Edit(String id)
        {
            User user = _UserUnitOfWork.FindById(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // POST: Book/Edit/5
        [HttpPost]
        public ActionResult Edit(User user)
        {
            try
            {
                // TODO: Add update logic here
                _UserUnitOfWork.Update(user);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Delete/5
        public ActionResult Delete(String id)
        {
            var user = _UserUnitOfWork.FindById(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // POST: Book/Delete/5
        [CustomAuthorization(IdentityRoles = "Admin")]
        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection)
        {
            _UserUnitOfWork.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
