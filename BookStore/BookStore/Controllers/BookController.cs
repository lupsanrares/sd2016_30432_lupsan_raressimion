﻿using BookStore.Models;
using BookStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using BookStore.App_Start;
using Bookstore.DataAccess;
namespace BookStore.Controllers
{
    
    public class BookController : Controller
    {
        IBookUnitOfWork _BookUnitOfWork;
        public BookController(IBookUnitOfWork bookUoW)
        {
            _BookUnitOfWork = bookUoW;
        }

        [CustomAuthorization(IdentityRoles = "Regular_User,Admin")]
        // GET: Book
        public ActionResult Index(int page=1,int pageSize=10)
        {

            List<BookViewModel> booksToDisplay = _BookUnitOfWork.GetAll().ToList();
            ViewBag.SearchText = "";
            ViewBag.SearchOption = 0;
            return View(booksToDisplay.ToPagedList(page,10));
        }
        [CustomAuthorization(IdentityRoles = "Regular_User,Admin")]
        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
       
        // GET: Book/Create
         [CustomAuthorization(IdentityRoles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        [CustomAuthorization(IdentityRoles = "Admin")]
        [HttpPost]
        public ActionResult Create(BookViewModel book)
        {
            try
            {

                _BookUnitOfWork.Create(book);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Edit/5
        [CustomAuthorization(IdentityRoles = "Admin")]
        public ActionResult Edit(int id)
        {
            BookViewModel book = _BookUnitOfWork.GetById(id);
            if (book == null)
            {
                return RedirectToAction("Index");
            }
        
            return View(book);
        }

        // POST: Book/Edit/5
        [CustomAuthorization(IdentityRoles = "Admin")]
        [HttpPost]
        public ActionResult Edit(BookViewModel book)
        {
            try
            {
                // TODO: Add update logic here
                _BookUnitOfWork.Update(book);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: Book/Delete/5
        [CustomAuthorization(IdentityRoles = "Admin")]
        public ActionResult Delete(int id)
        {
            BookViewModel book = _BookUnitOfWork.GetById(id);
            if (book == null)
            {
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // POST: Book/Delete/5
        [CustomAuthorization(IdentityRoles = "Admin")]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            _BookUnitOfWork.Delete(id);
            return RedirectToAction("Index");
        }

        [CustomAuthorization(IdentityRoles = "Regular_User,Admin")]
        public ActionResult Search(String searchText = "", int searchOption = 0, int page = 1)
        {
            List<BookViewModel> books = new List<BookViewModel>();

            switch (searchOption)
            {
                case 0:
                    books.AddRange(_BookUnitOfWork.SearchByTitle(searchText).ToList());
                    break;
                case 1:
                    books.AddRange(_BookUnitOfWork.SearchByAuthor(searchText).ToList());
                    break;
                case 2:
                    books.AddRange(_BookUnitOfWork.SearchByGenre(searchText).ToList());
                    break;
            }
            ViewBag.SearchText = searchText;
            ViewBag.SearchOption = searchOption;
            return View("Index", books.ToPagedList(page, 10));
        }
        [CustomAuthorization(IdentityRoles = "Regular_User,Admin")]
        public ActionResult Sell(int id)
        {
            BookViewModel book = _BookUnitOfWork.GetById(id);
            return View(book);
        }
        
        [HttpPost]
        [CustomAuthorization(IdentityRoles = "Regular_User,Admin")]
        public ActionResult Sell(BookViewModel book)
        {
            _BookUnitOfWork.Sell(book);
            return RedirectToAction("Index");
        }
        [CustomAuthorization(IdentityRoles = "Admin")]
        public ActionResult GenerateCSV()
        {
            String fileName=_BookUnitOfWork.GenerateReport("csv");
             System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        response.ClearContent();
        response.Clear();
        response.ContentType = "text/plain";
        response.AddHeader("Content-Disposition", 
                           "attachment; filename=" + fileName + ";");
        response.TransmitFile(fileName);
        response.Flush();    
        response.End();
            return RedirectToAction("Index");
        }
        [CustomAuthorization(IdentityRoles = "Admin")]
        public ActionResult GeneratePDF()
        {
            String fileName = _BookUnitOfWork.GenerateReport("pdf");
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition",
                               "attachment; filename=" + fileName + ";");
            response.TransmitFile(fileName);
            response.Flush();
            response.End();
            return RedirectToAction("Index");
        }

    }
}
