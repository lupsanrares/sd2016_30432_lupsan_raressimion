﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BookStore.App_Start
{
    public class InjectionConfig : Autofac.Module
    {

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("BookStore.DataAccess"))

                   .Where(t => t.Name.EndsWith("UnitOfWork"))

                   .AsImplementedInterfaces()

                   .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("BookStore.DataAccess"))

                   .Where(t => t.Name.EndsWith("Repository"))

                   .AsImplementedInterfaces()

                   .InstancePerLifetimeScope();

        }

    }
}