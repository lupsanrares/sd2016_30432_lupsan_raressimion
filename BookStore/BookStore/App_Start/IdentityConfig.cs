﻿//using System.Threading.Tasks;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin;
//using Microsoft.Owin.Security;
//using System.Security.Claims;
//using BookStore.Models;
//using System.Web;
//using BookStore.App_Start;
//using System;
//using System.Collections.Generic;

//namespace BookStore
//{
//    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

//    public class ApplicationUserManager : UserManager<User>
//    {
//        public ApplicationUserManager(IUserStore<User> store)
//            : base(store)
//        {
//        }
//        public override Task<ClaimsIdentity> CreateIdentityAsync(User user, string authenticationType)
//        {
//            ClaimsIdentity claims = new ClaimsIdentity(authenticationType);
//            claims.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", user.UserName));
//            claims.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", user.UserName));
//            return Task.FromResult(claims);
//        }

//        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
//        {
//            var manager = new ApplicationUserManager(new XmlUserStore());
//            var dataProtectionProvider = options.DataProtectionProvider;
//            if (dataProtectionProvider != null)
//            {
//                manager.UserTokenProvider = new DataProtectorTokenProvider<User>(dataProtectionProvider.Create("ASP.NET Identity"));
//            }
//            return manager;
//        }
   
//    public override Task<User> FindAsync(string userName, string password)
//        {
			
//            return base.FindAsync(userName, password);
//        }

//        public override Task<User> FindByEmailAsync(string email)
//        {

//            return base.FindByEmailAsync(email);
//        }
		
//        public override Task<User> FindByNameAsync(string userName)
//        {

//            return base.FindByNameAsync(userName);
//        }

//        protected override Task<bool> VerifyPasswordAsync(IUserPasswordStore<User, string> store, User user, string password)
//        {

//            return base.VerifyPasswordAsync(store, user, password);			
//        }

//        public override Task<bool> IsLockedOutAsync(string userId)
//        {

//            return base.IsLockedOutAsync(userId);			
//        }

//        public override Task<bool> GetTwoFactorEnabledAsync(string userId)
//        {

//            return Task.FromResult<bool>(false);
//        }

//        public override Task<string> GetSecurityStampAsync(string userId)
//        {
//            throw new NotImplementedException();
//        }

//        public override Task<IList<string>> GetRolesAsync(string userId)
//        {
//            throw new NotImplementedException();			
//        }

//        public override Task<IList<Claim>> GetClaimsAsync(string userId)
//        {
//            throw new NotImplementedException();
//        }
//    }
//    public class ApplicationSignInManager : SignInManager<User, string>
//    {
//        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
//            : base(userManager, authenticationManager)
//        {
//        }

//        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
//        {
//            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
//        }
       
//        public override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
//        {
//            return base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);
//        }

//        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
//        {
//            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
//        }
//    }
//    public class EmailService : IIdentityMessageService
//    {
//        public Task SendAsync(IdentityMessage message)
//        {
//            // Plug in your email service here to send an email.
//            return Task.FromResult(0);
//        }
//    }

//    public class SmsService : IIdentityMessageService
//    {
//        public Task SendAsync(IdentityMessage message)
//        {
//            // Plug in your sms service here to send a text message.
//            return Task.FromResult(0);
//        }
//    }

//}