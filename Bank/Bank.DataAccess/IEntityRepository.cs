﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public interface IEntityRepository<T,TDTO> : IRepository
     where T : class where TDTO:class
    {
        void Create(TDTO entity);
        void Delete(TDTO entity);
        IEnumerable<TDTO> GetAll();
        void Update(TDTO entity);
    }
}
