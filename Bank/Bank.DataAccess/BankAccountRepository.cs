﻿using AutoMapper;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public class BankAccountRepository : EntityRepository<BankAccount,BankAccountDTO>, IBankAccountRepository
    {
        IContext _context;
        public BankAccountRepository(IContext context)
            : base(context)
        {
            _context = context;
            _dbset = _context.Set<BankAccount>();
        }

        public override IEnumerable<BankAccountDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<BankAccount>, IEnumerable<BankAccountDTO>>(_context.BankAccounts).ToList();
        }

        public BankAccountDTO GetById(string iban)
        {
            return Mapper.Map<BankAccountDTO>(_dbset.FirstOrDefault(x => x.IBAN.Equals(iban)));
            
        }

        public override void Create(BankAccountDTO account)
        {
            if (account == null)
            {
                return;
            }
            if (GetById(account.IBAN) == null)
            {

                var c = Mapper.Map<BankAccount>(account); 
                _dbset.Add(c);
                _context.Entry(c).State = EntityState.Added; 
                _context.SaveChanges();
            }
        }
        public override void Update(BankAccountDTO account)
        {
            if (account == null)
            {
                return;
            }
            var accountDTO = _dbset.FirstOrDefault(x => x.IBAN.Equals(account.IBAN));
            if (account != null)
            {
                var c = Mapper.Map<BankAccount>(account);
                _context.Entry(accountDTO).CurrentValues.SetValues(c);
                _context.Entry(accountDTO).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
        public override void Delete(BankAccountDTO account)
        {
            if (account == null)
            {
                return;
            }
            var accountDTO = _dbset.FirstOrDefault(x => x.IBAN.Equals(account.IBAN));
            if (accountDTO != null)
            {
                _dbset.Remove(accountDTO);
                _context.Entry(accountDTO).State = EntityState.Deleted;
                _context.SaveChanges();
            }
        }
    }
}
