﻿using Bank.DataAccess;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.BusinessLogic
{
    public class BankAccountUnitOfWork:IBankAccountUnitOfWork
    {
        #region Fields
        private readonly bankEntities _context;
        private IClientRepository _clientRepository;
        private IBankAccountRepository _accountRepository;
        private IReportRepository _reportRepository;
        #endregion

        #region Constructors
        public BankAccountUnitOfWork(IClientRepository repository, IBankAccountRepository accountRepository, IReportRepository reportRepository)
        {
            _context = new bankEntities();
            _clientRepository= repository;
            _accountRepository=accountRepository;
            _reportRepository = reportRepository;
        }
        #endregion

        #region IUnitOfWork Members
        public IClientRepository ClientRepository
        {
            get { return _clientRepository ?? (_clientRepository = new ClientRepository(_context)); }
        }
         public IBankAccountRepository BankAccountRepository
        {
            get { return _accountRepository ?? (_accountRepository = new BankAccountRepository(_context)); }
        }

        public IEnumerable<BankAccountDTO> GetAll()
        {
            return _accountRepository.GetAll();
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _clientRepository = null;
            _accountRepository = null;
            _reportRepository = null;
            _context.Dispose();
        }
        #endregion


        public void Create(BankAccountDTO entity)
        {
            _accountRepository.Create(entity);
        }

        public void Delete(BankAccountDTO entity)
        {
            _accountRepository.Delete(entity);
        }

        public void Update(BankAccountDTO entity)
        {
            _accountRepository.Update(entity);
        }

        public BankAccountDTO GetById(string iban)
        {
            return _accountRepository.GetById(iban);
        }
        public void Report(ReportDTO report)
        {
            _reportRepository.Create(report);
        }
    }
    
}
