﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public interface IUserUnitOfWork:IUnitOfWork<UserDTO>
    {
        UserDTO GetById(string Id);
        UserDTO GetByUsername(string Id);
        IEnumerable<ReportDTO> GetByDate(String Id,DateTime first, DateTime second);
    }
}
