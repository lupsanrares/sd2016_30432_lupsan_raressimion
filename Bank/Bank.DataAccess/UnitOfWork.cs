﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public abstract class UnitOfWork<T> where T : class //: IUnitOfWork<T>
    {
        ///// <summary>
        ///// The DbContext
        ///// </summary>
        //private bankEntities _dbContext;
        //private IReportRepository _reportRepository;
        ///// <summary>
        ///// Initializes a new instance of the UnitOfWork class.
        ///// </summary>
        ///// <param name="context">The object context</param>
        //public UnitOfWork(bankEntities context, IReportRepository reportRepository)
        //{
        //    _reportRepository = reportRepository;
        //    _dbContext = context;
        //}



        ///// <summary>
        ///// Saves all pending changes
        ///// </summary>
        ///// <returns>The number of objects in an Added, Modified, or Deleted state</returns>
        //public int Commit()
        //{
        //    // Save changes with the default options
        //    return _dbContext.SaveChanges();
        //}

        ///// <summary>
        ///// Disposes the current object
        ///// </summary>
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        ///// <summary>
        ///// Disposes all external resources.
        ///// </summary>
        ///// <param name="disposing">The dispose indicator.</param>
        //private void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        if (_dbContext != null)
        //        {
        //            _dbContext.Dispose();
        //            _dbContext = null;
        //        }
        //    }
        //}

        //int IUnitOfWork<T>.SaveChanges()
        //{
        //    throw new NotImplementedException();
        //}

        //void IUnitOfWork<T>.Create(T entity)
        //{
        //    throw new NotImplementedException();
        //}

        //void IUnitOfWork<T>.Delete(T entity)
        //{
        //    throw new NotImplementedException();
        //}

        //IEnumerable<T> IUnitOfWork<T>.GetAll()
        //{
        //    throw new NotImplementedException();
        //}

        //void IUnitOfWork<T>.Update(T entity)
        //{
        //    throw new NotImplementedException();
        //}

        //void IDisposable.Dispose()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
