﻿using Bank.DataAccess;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.BusinessLogic
{
    public class UserUnitOfWork:IUserUnitOfWork
    {
        #region Fields
        private readonly bankEntities _context;
        private IUserRepository _userRepository;
        private IReportRepository _reportRepository;
        #endregion

        #region Constructors
        public UserUnitOfWork(IUserRepository repository, IReportRepository reportRepository)
        {
            _context = new bankEntities();
            _userRepository = repository;
            _reportRepository = reportRepository;
        }
        #endregion

        #region IUnitOfWork Members
        public IUserRepository UserRepository
        {
            get { return _userRepository ?? (_userRepository = new UserRepository(_context)); }
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _userRepository.GetAll();
        }
        public IEnumerable<ReportDTO> GetByDate(String Id,DateTime first, DateTime second)
        {
            return _reportRepository.GetByDate(Id,first,second);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            _userRepository = null;
            _reportRepository = null;
            _context.Dispose();
        }
        #endregion


        public void Create(UserDTO entity)
        {
            _userRepository.Create(entity);
        }

        public void Delete(UserDTO entity)
        {
            _userRepository.Delete(entity);
        }

        public void Update(UserDTO entity)
        {
            _userRepository.Update(entity);
        }

        public UserDTO GetById(string Id)
        {
            return _userRepository.GetById(Id);
        }
        public UserDTO GetByUsername(string Id)
        {
            return _userRepository.GetByUsername(Id);
        }
        public void Report(ReportDTO report)
        {
            _reportRepository.Create(report);
        }
    }
    
}
