﻿using AutoMapper;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public class ReportRepository : EntityRepository<Report, ReportDTO>, IReportRepository
    {
        IContext _context;
        public ReportRepository(IContext context)
            : base(context)
        {
            _context = context;
            _dbset = _context.Set<Report>();
        }

        public override IEnumerable<ReportDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<Report>, IEnumerable<ReportDTO>>(_context.Reports).ToList();
        }

        public IEnumerable<ReportDTO> GetByDate(String Id,DateTime first, DateTime second)
        {
            return Mapper.Map<IEnumerable<Report>,IEnumerable<ReportDTO>>(_dbset.Where(t => t.UserId.Equals(Id)&&(DateTime.Compare(t.Creation, first) >= 0) && (DateTime.Compare(second, t.Creation) >= 0)));

        }

        public override void Create(ReportDTO report)
        {
                var c = Mapper.Map<Report>(report);
                _dbset.Add(c);
                _context.SaveChanges();
        }
    }
}
