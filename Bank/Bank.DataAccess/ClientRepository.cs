﻿using AutoMapper;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public class ClientRepository : EntityRepository<Client,ClientDTO>, IClientRepository
    {
        IContext _context;
        public ClientRepository(IContext context)
            : base(context)
        {
            _context = context;
            _dbset = _context.Set<Client>();
        }

        public override IEnumerable<ClientDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<Client>, IEnumerable<ClientDTO>>(_context.Clients).ToList();
        }

        public ClientDTO GetById(string pnc)
        {
            return Mapper.Map<ClientDTO>(_dbset.FirstOrDefault(x => x.PNC.Equals(pnc)));
            
        }

        public override void Create(ClientDTO client)
        {
            if (client == null)
            {
                return;
            }
            if (GetById(client.PNC) == null)
            {

                var c = Mapper.Map<Client>(client); 
                _dbset.Add(c);
                _context.Entry(c).State = EntityState.Added; 
                _context.SaveChanges();
            }
        }
        public override void Update(ClientDTO client)
        {
            if (client == null)
            {
                return;
            }
            var clientDTO=_dbset.FirstOrDefault(x => x.PNC.Equals(client.PNC));
            if ( clientDTO!= null)
            {
                var c = Mapper.Map<Client>(client);
                _context.Entry(clientDTO).CurrentValues.SetValues(c);
                _context.Entry(clientDTO).State=EntityState.Modified;
                _context.SaveChanges();
            }
        }
        public override void Delete(ClientDTO client)
        {
            if (client == null)
            {
                return;
            }
            var clientDTO = _dbset.FirstOrDefault(x => x.PNC.Equals(client.PNC));
            if (clientDTO != null)
            {
                _dbset.Remove(clientDTO);
                _context.Entry(clientDTO).State = EntityState.Deleted;
                _context.SaveChanges();
            }
        }
    }
}
