﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public interface IClientRepository:IEntityRepository<Client,ClientDTO>
    {
        ClientDTO GetById(string Id);
    }
}
