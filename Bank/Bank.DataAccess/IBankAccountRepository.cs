﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.DataAccess
{
    public interface IBankAccountRepository : IEntityRepository<BankAccount, BankAccountDTO>
    {
        BankAccountDTO GetById(string iban);
    }
}
