﻿using Bank.DataAccess;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bank.BusinessLogic
{
    public class BankAccountService : Service<BankAccountDTO>, IBankAccountService
    {
         IBankAccountUnitOfWork _account;
         IUserUnitOfWork _user;
         public BankAccountService(IBankAccountUnitOfWork account,  IUserUnitOfWork user)
            : base(account)
        {
            _account = account;
            _user = user;
        }

        public override IEnumerable<BankAccountDTO> GetAll()
        {
            return _account.GetAll().ToList();
        }

        public BankAccountDTO GetById(string iban)
        {
            return _account.GetById(iban);
        }

        public override void Create(BankAccountDTO account)
        {
            if (account == null)
            {
                return;
            }
            if (GetById(account.IBAN) == null)
            {
                _account.Create(account);
                var username = Thread.CurrentPrincipal.Identity.Name;

                ReportDTO report = new ReportDTO()
                {
                    UserId = _user.GetByUsername(username).Id,
                    Description = "Employee " + username + " created Bank account " + account.IBAN + " for user with PNC:" + account.ClientPNC,
                    Creation = DateTime.Now
                };
                _account.Report(report);
                _account.SaveChanges();
            }
        }

        public override void Update(BankAccountDTO account)
        {
            if (account == null) throw new ArgumentNullException("entity");
            _account.Update(account);
            var username = Thread.CurrentPrincipal.Identity.Name;

            ReportDTO report = new ReportDTO()
            {
                UserId = _user.GetByUsername(username).Id,
                Description = "Employee " + username + " updated Bank account " + account.IBAN + " for user with PNC:" + account.ClientPNC,
                Creation = DateTime.Now
            };
            _account.Report(report);
            _account.SaveChanges();
        }
        public override void Delete(BankAccountDTO account)
        {
            if (account == null) throw new ArgumentNullException("entity");
            _account.Delete(account);
            var username = Thread.CurrentPrincipal.Identity.Name;

            ReportDTO report = new ReportDTO()
            {
                UserId = _user.GetByUsername(username).Id,
                Description = "Employee " + username + " deleted Bank account " + account.IBAN + " for user with PNC:" + account.ClientPNC,
                Creation = DateTime.Now
            };
            _account.Report(report);
            _account.SaveChanges();
        }
        public void ProcessBill(Bill bill)
        {
            if (bill == null)
            {
                return;
            }
            var account = _account.GetById(bill.IBAN);
            account.Balance -= bill.Value;
            _account.Update(account);
            var username = Thread.CurrentPrincipal.Identity.Name;

            ReportDTO report = new ReportDTO()
            {
                UserId = _user.GetByUsername(username).Id,
                Description = "Employee " + username + " Processed Bill "+bill.BillCode +" using Bank account " + account.IBAN + " for user with PNC:" + account.ClientPNC,
                Creation = DateTime.Now
            };
            _account.Report(report);
            _account.SaveChanges();
        }
        public void TransferMoney(Transfer transfer)
        {
            if (transfer == null)
            {
                return;
            }
            var account = _account.GetById(transfer.Source);
            account.Balance -= transfer.Amount;
            _account.Update(account);
            account = _account.GetById(transfer.Destination);
            account.Balance += transfer.Amount;
            _account.Update(account);
            var username = Thread.CurrentPrincipal.Identity.Name;

            ReportDTO report = new ReportDTO()
            {
                UserId = _user.GetByUsername(username).Id,
                Description = "Employee " + username + " transfered  "+transfer.Amount+" from account " + transfer.Source+ " to account " + transfer.Destination,
                Creation = DateTime.Now
            };
            _account.Report(report);
            _account.SaveChanges();
        }
    }
}
