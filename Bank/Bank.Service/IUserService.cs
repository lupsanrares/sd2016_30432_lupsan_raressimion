﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.BusinessLogic
{
    public interface IUserService : IService<UserDTO>
    {
        UserDTO GetById(string Id);
        IEnumerable<ReportDTO> GetByDate(String Id, DateTime first, DateTime second);
    }
}
