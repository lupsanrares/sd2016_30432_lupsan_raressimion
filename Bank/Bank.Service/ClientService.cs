﻿using Bank.Model;
using Bank.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.DataAccess;
using System.Threading;

namespace Bank.BusinessLogic
{
    public class ClientService : Service<ClientDTO>, IClientService
    {
        IClientUnitOfWork _client;
        IUserUnitOfWork _user;
        public ClientService(IClientUnitOfWork client, IUserUnitOfWork user)
            : base(client)
        {
            _client = client;
            _user = user;

        }

        public override IEnumerable<ClientDTO> GetAll()
        {
            return _client.GetAll().ToList();
        }

        public ClientDTO GetById(string pnc)
        {
            return _client.GetById(pnc);
        }

        public override void Create(ClientDTO client)
        {
            if (client == null)
            {
                return;
            }
            if (GetById(client.PNC) == null)
            {
                _client.Create(client);
                var username = Thread.CurrentPrincipal.Identity.Name;
                
                ReportDTO report = new ReportDTO()
                {
                    UserId=_user.GetByUsername(username).Id,
                    Description = "Employee " + username + " created user " + client.Name + " " + client.PNC,
                    Creation = DateTime.Now
                };
                _client.Report(report);
                _client.SaveChanges();
            }
        }

        public override void Delete(ClientDTO entity)
        {
            if (entity == null)
            {
                return;
            }
            if (GetById(entity.PNC)!= null)
            {
                _client.Delete(entity);
                var username = Thread.CurrentPrincipal.Identity.Name;

                ReportDTO report = new ReportDTO()
                {
                    UserId = _user.GetByUsername(username).Id,
                    Description = "Employee " + username + " deleted user " + entity.Name + " " + entity.PNC,
                    Creation = DateTime.Now
                };
                _client.Report(report);
                _client.SaveChanges();
            }
        }

        public override void Update(ClientDTO entity)
        {
            if (entity == null)
            {
                return;
            }
            if (GetById(entity.PNC) != null)
            {
                _client.Update(entity);
                var username = Thread.CurrentPrincipal.Identity.Name;

                ReportDTO report = new ReportDTO()
                {
                    UserId = _user.GetByUsername(username).Id,
                    Description = "Employee " + username + " updated user " + entity.Name + " " + entity.PNC,
                    Creation = DateTime.Now
                };
                _client.Report(report);
                _client.SaveChanges();
            }
        }
    }
}
