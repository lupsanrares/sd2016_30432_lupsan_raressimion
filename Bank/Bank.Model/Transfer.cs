﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class Transfer
    {
        public String  Source { get; set; }
        public String Destination { get; set; }
        public Double Balance { get; set; }
        public Double Amount { get; set; }
    }
}
