﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class BankAccountDTO
    {
        [Required]
        [StringLength(13, MinimumLength = 13)]
        public String ClientPNC { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 24)]
        [RegularExpression("[a-zA-Z]{2}[0-9]{2}[a-zA-Z]{4}[0-9]{16}")]
        public String IBAN { get; set; }
        public String Type { get; set; }
        [Range(0.0,Double.MaxValue)]
        public Double Balance { get; set; }
        [DataType(DataType.Date)]
        public DateTime Creation { get; set; }
    }
}
