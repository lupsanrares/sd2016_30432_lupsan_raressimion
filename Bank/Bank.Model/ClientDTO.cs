﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Model
{
    public class ClientDTO
    {
        [Required]
        [StringLength(13,MinimumLength=13)]
        [RegularExpression("[0-9]*")]
        public String PNC { get; set; }

        [Required]
        [StringLength(255,MinimumLength=5)]
        public String Name { get; set; }

        [Required]
        [StringLength(8,MinimumLength=8)]
        [RegularExpression("[a-zA-Z]{2}[0-9]{6}")]
        public String ICN { get; set; }

        [Required]
        [StringLength(255)]
        public String Address { get; set; }

        [Required]
        [StringLength(10,MinimumLength=10)]
        [RegularExpression("[0-9]*")]
        public String Phone { get; set; }
    }
}
