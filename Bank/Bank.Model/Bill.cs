﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bank.Model
{
    public class Bill
    {
        public String IBAN { get; set; }
        public double Balance { get; set; }
        [Range(0,Double.MaxValue,ErrorMessage="Value can not be less than 0")]
        public double Value { get; set; }
        [Required]
        public String BillCode { get; set; }
    }
}