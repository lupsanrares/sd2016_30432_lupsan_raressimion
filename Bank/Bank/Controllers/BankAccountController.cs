﻿using Bank.BusinessLogic;
using Bank.Model;
using Bank.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Bank.Controllers
{
    [Authorize(Roles="Admin, Regular_User")]
    public class BankAccountController : Controller
    {
        IBankAccountService _BankAccountService;
        IClientService _ClientService;

        public BankAccountController(IBankAccountService BankAccountService,IClientService ClientService)
        {
            _BankAccountService = BankAccountService;
            _ClientService = ClientService;

        }
        // GET: BankAccount

        public ActionResult Index()
        {
            return View(_BankAccountService.GetAll());
        }

        // GET: BankAccount/Details/5
        public ActionResult Details(String iban)
        {
            return View();
        }

        // GET: BankAccount/Create
        public ActionResult Create()
        {
            var clients=_ClientService.GetAll().ToList();
            return View(new CreateAccountViewModel()
            {
                Clients = clients
            });
        }

        // POST: BankAccount/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {

                DateTime date = DateTime.ParseExact(collection.GetValue("Creation").AttemptedValue,
                                        "yyyy-MM-dd",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None);
               // var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                _BankAccountService.Create(new BankAccountDTO() {ClientPNC=collection.GetValue("ClientPNC").AttemptedValue, IBAN = collection.GetValue("IBAN").AttemptedValue, Type = collection.GetValue("Type").AttemptedValue, Balance = Double.Parse(collection.GetValue("Balance").AttemptedValue), Creation = date });
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BankAccount/Edit/5
        public ActionResult Edit(String iban)
        {
            return View(_BankAccountService.GetById(iban));
        }

        // POST: BankAccount/Edit/5
        [HttpPost]
        public ActionResult Edit(String iban, FormCollection collection)
        {
            try
            {
                DateTime date = DateTime.Parse(collection.GetValue("Creation").AttemptedValue
                                        );
                BankAccountDTO x = new BankAccountDTO() { ClientPNC = collection.GetValue("ClientPNC").AttemptedValue, IBAN = collection.GetValue("IBAN").AttemptedValue, Type = collection.GetValue("Type").AttemptedValue, Balance = Double.Parse(collection.GetValue("Balance").AttemptedValue), Creation = date };

                _BankAccountService.Update(x);


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BankAccount/Delete/5
        public ActionResult Delete(String iban)
        {
            return View(_BankAccountService.GetById(iban));
        }

        // POST: BankAccount/Delete/5
        [HttpPost]
        public ActionResult Delete(String iban, FormCollection collection)
        {
            try
            {
                DateTime date = DateTime.ParseExact(collection.GetValue("Creation").AttemptedValue,
                                       "yyyy-MM-dd",
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.None);

                _BankAccountService.Delete(new BankAccountDTO() { ClientPNC = collection.GetValue("ClientPNC").AttemptedValue, IBAN = collection.GetValue("IBAN").AttemptedValue, Type = collection.GetValue("Type").AttemptedValue, Balance = Double.Parse(collection.GetValue("Balance").AttemptedValue), Creation = date });
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ProcessBill(String iban)
        {
            var account=_BankAccountService.GetById(iban);
            return View(new Bill() { IBAN = account.IBAN, Balance = account.Balance });
        }

        // POST: BankAccount/Delete/5
        [HttpPost]
        public ActionResult ProcessBill(Bill bill)
        {
            try
            {
                _BankAccountService.ProcessBill(bill);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BankAccount/Delete/5
        public ActionResult Transfer(String iban)
        {
            List<BankAccountDTO> accounts=_BankAccountService.GetAll().ToList();
            BankAccountDTO source=_BankAccountService.GetById(iban);
            accounts = accounts.Where(t => !t.IBAN.Equals(iban)).ToList();
            TransferMoneyViewModel model = new TransferMoneyViewModel()
            {
                Source = source.IBAN,
                Accounts = accounts,
                Balance=source.Balance
            };
            return View(model);
        }

        // POST: BankAccount/Delete/5
        [HttpPost]
        public ActionResult Transfer(Transfer transfer)
        {
            try
            {
                _BankAccountService.TransferMoney(transfer);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
