﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Bank.BusinessLogic;
using AutoMapper;
using Bank.Model;
using Bank.Models;

namespace Bank.Controllers
{
    [Authorize(Roles="Admin")]
    public class AdminController:Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private IUserService _UserService;
     
        public AdminController(IUserService UserService)
        {
            _UserService = UserService;
        }

        public ApplicationUserManager UserManager {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return this._roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set { this._roleManager = value; }
        }
        public ActionResult Index()
        {
            var employees=Mapper.Map<IEnumerable<UserDTO>, IEnumerable<EmployeeViewModel>>(_UserService.GetAll()).ToList();
            return View(employees);
        }
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    var userInserted = await UserManager.FindByEmailAsync(model.Email);
                    await UserManager.AddToRoleAsync(userInserted.Id, "Regular_User");
                }
            }
            return RedirectToAction("Index", "Admin");
        }

        public ActionResult Edit(String Id)
        {
            var employee = Mapper.Map<UserDTO, EmployeeViewModel>(_UserService.GetById(Id));
            return View(employee);
        }

        // POST: Client/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(EmployeeViewModel employee)
        {
            try
            {
                var userInserted = await UserManager.FindByEmailAsync(employee.Email);
                if(userInserted==null)
                    _UserService.Update(new UserDTO() {Id=employee.Id,Email=employee.Email,PhoneNumber=employee.PhoneNumber });
                else
                    return View(employee);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(employee);
            }
        }
        public ActionResult Delete(String Id)
        {
            var employee = Mapper.Map<UserDTO, EmployeeViewModel>(_UserService.GetById(Id));
            return View(employee);
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(EmployeeViewModel employee)
        {
            try
            {

                _UserService.Delete(new UserDTO() { Id = employee.Id, Email = employee.Email, PhoneNumber = employee.PhoneNumber });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult ShowReports(String Id)
        {
            return View(new ReportViewModel() { Id = Id });
        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult ShowReports(ReportViewModel report)
        {
            return View("Reports",_UserService.GetByDate(report.Id, report.First, report.Second));
        }
    }
}