﻿using Bank.Model;
using Bank.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bank.Controllers
{
    [Authorize(Roles="Admin, Regular_User")]
    public class ClientController : Controller
    {
        IClientService _ClientService;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        public ClientController(IClientService ClientService)
        {
            _ClientService = ClientService;

        }

        // GET: Client
        public ActionResult Index()
        {
            return View(_ClientService.GetAll());
        }

        // GET: Client/Details/5
        public ActionResult Details(String pnc)
        {
            return View();
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                _ClientService.Create(new ClientDTO() { PNC = collection.GetValue("PNC").AttemptedValue, Name = collection.GetValue("Name").AttemptedValue, Phone = collection.GetValue("Phone").AttemptedValue, ICN = collection.GetValue("ICN").AttemptedValue, Address = collection.GetValue("Address").AttemptedValue });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Edit/5
        public ActionResult Edit(String pnc)
        {
            return View(_ClientService.GetById(pnc));
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(String pnc, FormCollection collection)
        {
            try
            {
                _ClientService.Update(new ClientDTO() { PNC = collection.GetValue("PNC").AttemptedValue, Name = collection.GetValue("Name").AttemptedValue, Phone = collection.GetValue("Phone").AttemptedValue, ICN = collection.GetValue("ICN").AttemptedValue, Address = collection.GetValue("Address").AttemptedValue });

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(String pnc)
        {
            
            return View(_ClientService.GetById(pnc));
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(String pnc, FormCollection collection)
        {
            try
            {

                _ClientService.Delete(new ClientDTO() { PNC = collection.GetValue("PNC").AttemptedValue, Name = collection.GetValue("Name").AttemptedValue, Phone = collection.GetValue("Phone").AttemptedValue, ICN = collection.GetValue("ICN").AttemptedValue, Address = collection.GetValue("Address").AttemptedValue });
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}