﻿using Bank.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class DisplayAccountViewModel
    {
        [Required]
        [StringLength(13, MinimumLength = 13)]
        public String ClientPNC { get; set; }
        [Required]
        [StringLength(24, MinimumLength = 24)]
        [RegularExpression("[a-zA-Z]{2}[0-9]{2}[a-zA-Z]{4}[0-9]{16}")]
        public String IBAN { get; set; }
        public String Type { get; set; }
        [Range(0.0,Double.MaxValue)]
        public Double Balance { get; set; }
        [DataType(DataType.Date)]
        public DateTime Creation { get; set; }
    }

    public class CreateAccountViewModel
    {
        public IEnumerable<ClientDTO> Clients { get; set; }
        [Required]
        //[StringLength(13, MinimumLength = 13)]
        public String ClientPNC { get; set; }
        [Required]
        [StringLength(24, MinimumLength = 24)]
        [RegularExpression("[a-zA-Z]{2}[0-9]{2}[a-zA-Z]{4}[0-9]{16}")]
        public String IBAN { get; set; }
        public String Type { get; set; }
        [Range(0.0, Double.MaxValue)]
        public Double Balance { get; set; }
        [DataType(DataType.Date)]
        public DateTime Creation { get; set; }
    }
    public class TransferMoneyViewModel
    {
        public IEnumerable<BankAccountDTO> Accounts { get; set; }
        public String Source { get; set; }
        public String Destination { get; set; }
        [Required]
        [Range(0,Double.MaxValue)]
        public Double Amount { get; set; }
        public Double Balance { get; set; }

    }
}