﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class ReportViewModel
    {
        public String Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime First { get; set; }
        [DataType(DataType.Date)]
        public DateTime Second { get; set; }
    }
}