﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Bank.Modules
{
    public class ServiceModule : Autofac.Module
    {

        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterAssemblyTypes(Assembly.Load("Bank.BusinessLogic"))

                      .Where(t => t.Name.EndsWith("Service"))

                      .AsImplementedInterfaces()

                      .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("Bank.DataAccess"))

                    .Where(t => t.Name.EndsWith("UnitOfWork"))

                    .AsImplementedInterfaces()

                    .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("Bank.DataAccess"))

                   .Where(t => t.Name.EndsWith("Repository"))

                   .AsImplementedInterfaces()

                   .InstancePerLifetimeScope();

        }

    }
}