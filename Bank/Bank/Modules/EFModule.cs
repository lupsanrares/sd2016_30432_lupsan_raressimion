﻿using Autofac;
using Bank.DataAccess;
using Bank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Modules
{
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType(typeof(bankEntities)).As(typeof(IContext)).InstancePerLifetimeScope();

        }

    }
}