namespace Bank.Migrations
{
    using Bank.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Bank.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Bank.Models.ApplicationDbContext";
        }

        protected override void Seed(Bank.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            ApplicationUser admin = new ApplicationUser()
            {
                Id = "720f77f7-33ac-4b43-86f1-b0f2b0a7e220",
                Email = "lupsanrares@yahoo.com",
                PasswordHash = "+0kECRXAXeoOm2hkWJpA==",
                EmailConfirmed = false,
                SecurityStamp = "ec4a3853-5d5a-4a6d-9c5c-ab225246ee16"


            };
            context.Roles.AddOrUpdate(new IdentityRole("Admin"));
            context.Roles.AddOrUpdate(new IdentityRole("Regular_User"));
           // context.Users.AddOrUpdate()
        }
    }
}
