﻿using AutoMapper;
using Bank.DataAccess;
using Bank.Model;
using Bank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.App_Start
{
    public static class InitializeAutoMapper
    {
        public static void Initialize()
        {
            Mapper.CreateMap<Client, ClientDTO>();
            Mapper.CreateMap<ClientDTO, Client>();
            Mapper.CreateMap<Report, ReportDTO>();
            Mapper.CreateMap<ReportDTO, Report>();
            Mapper.CreateMap<BankAccount, BankAccountDTO>();
            Mapper.CreateMap<BankAccountDTO, BankAccount>();
            Mapper.CreateMap<AspNetUser, UserDTO>();
            Mapper.CreateMap<UserDTO, AspNetUser>();
            Mapper.CreateMap<EmployeeViewModel, UserDTO>();
            Mapper.CreateMap<UserDTO, EmployeeViewModel>();
        }
    }
    
}