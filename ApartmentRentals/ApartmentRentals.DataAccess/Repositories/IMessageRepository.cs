﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public interface IMessageRepository:IEntityRepository<Message,MessageDTO>
    {
        MessageDTO GetById(string id);
        IEnumerable<string> GetAllConversations(string userId);
        IEnumerable<MessageDTO> GetConversation(string Id1, string Id2);
    }
}
