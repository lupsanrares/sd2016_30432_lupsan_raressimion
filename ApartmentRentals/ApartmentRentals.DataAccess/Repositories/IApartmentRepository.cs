﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public interface IApartmentRepository:IEntityRepository<Apartment,ApartmentDTO>
    {
        ApartmentDTO GetById(int id);
    }
}
