﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public interface IImagesRepository:IEntityRepository<Image,ImageDTO>
    {
        void Add(List<string> urls, int aptId);
        IEnumerable<ImageDTO> GetByAptId(int aptId);
    }
}
