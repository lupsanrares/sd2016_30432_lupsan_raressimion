﻿using ApartmentRentals.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public class AmenitiesRepository:EntityRepository<Facility,AmenitiesDTO>,IAmenitiesRepository
    {
        IContext _context;
        public AmenitiesRepository(IContext context)
             : base(context)
         {
             _context = context;
             _dbset = _context.Set<Facility>();
         }
        public AmenitiesDTO GetById(int id)
        {
            var apt = _dbset.Where(t => t.Id == id).FirstOrDefault();
            return Mapper.Map<AmenitiesDTO>(apt);
        }
    }
}
