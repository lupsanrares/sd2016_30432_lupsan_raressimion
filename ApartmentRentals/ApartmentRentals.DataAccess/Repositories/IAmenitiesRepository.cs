﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public interface IAmenitiesRepository:IEntityRepository<Facility,AmenitiesDTO>
    {
        AmenitiesDTO GetById(int id);
    }
}
