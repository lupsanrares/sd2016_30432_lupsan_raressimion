﻿using ApartmentRentals.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public class ImagesRepository:EntityRepository<Image,ImageDTO>,IImagesRepository
    {
          IContext _context;
          public ImagesRepository(IContext context)
             : base(context)
         {
             _context = context;
             _dbset = _context.Set<Image>();
         }
        public void Add(List<string> urls, int aptId)
        {
            int counter=1;
           foreach(var url in urls)
           {
               this.Create(new ImageDTO { Url = url, Apartment_Id = aptId, Name = counter.ToString() });
                   counter++;
           }
           this._context.SaveChanges();
        }
        public IEnumerable<ImageDTO> GetByAptId(int aptId)
        {
            var images=_dbset.Where(t => t.Apartment_Id == aptId);
            return Mapper.Map<IEnumerable<ImageDTO>>(images);
        }
    }
}
