﻿using ApartmentRentals.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public class OfferRepository:EntityRepository<Offer,OfferDTO>,IOfferRepository
    {
        IContext _context;
        public OfferRepository(IContext context)
             : base(context)
         {
             _context = context;
             _dbset = _context.Set<Offer>();
         }
        public OfferDTO GetById(int id)
        {
            var apt = _dbset.Where(t => t.Id==id).FirstOrDefault();
            return Mapper.Map<OfferDTO>(apt);
        }
        public IEnumerable<OfferDTO> GetAllOffers(string userId)
        {
            var list = _dbset.Where(t => t.Landlord_Id.Equals(userId) || t.Tenant_Id.Equals(userId)).ToList();
            return Mapper.Map<IEnumerable<OfferDTO>>(list);
        }
        public IEnumerable<OfferDTO> GetByAptId(int id)
        {
            var list = _dbset.Where(t => t.Apartment_Id==id).ToList();
            return Mapper.Map<IEnumerable<OfferDTO>>(list);
        }

        public override void Update(OfferDTO aptDTO)
        {
            if (aptDTO == null)
            {
                return;
            }
            var apt = _dbset.FirstOrDefault(x => x.Id == aptDTO.Id);
            if (apt != null)
            {
                var c = Mapper.Map<Offer>(aptDTO);
                _context.Entry(apt).CurrentValues.SetValues(c);
                _context.Entry(apt).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }
    }
}
