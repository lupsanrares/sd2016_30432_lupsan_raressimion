﻿using ApartmentRentals.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public class ApartmentRepository:EntityRepository<Apartment,ApartmentDTO>,IApartmentRepository
    {
         IContext _context;
         public ApartmentRepository(IContext context)
             : base(context)
         {
             _context = context;
             _dbset = _context.Set<Apartment>();
         }
         public override void Create(ApartmentDTO entityDTO)
         {
             if (entityDTO == null)
             {
                 throw new ArgumentNullException("entity");
             }
             var entity = Mapper.Map<Apartment>(entityDTO);

             _context.Entry(entity).State = EntityState.Added;
             _dbset.Add(entity);
             _context.SaveChanges();
             entityDTO.Id = entity.Id;
         }
         public override void Update(ApartmentDTO aptDTO)
         {
             if (aptDTO == null)
             {
                 return;
             }
             var apt = _dbset.FirstOrDefault(x => x.Id==aptDTO.Id);
             if (apt != null)
             {
                 var c = Mapper.Map<Apartment>(aptDTO);
                 _context.Entry(apt).CurrentValues.SetValues(c);
                 _context.Entry(apt).State = EntityState.Modified;
                 _context.SaveChanges();
             }
         }
         public ApartmentDTO GetById(int id)
         {
             var apt=_dbset.Where(t => t.Id == id).FirstOrDefault();
             return Mapper.Map<ApartmentDTO>(apt);
         }
    }
}
