﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public interface IOfferRepository:IEntityRepository<Offer,OfferDTO>
    {
        OfferDTO GetById(int id);
        IEnumerable<OfferDTO> GetAllOffers(string userId);
        IEnumerable<OfferDTO> GetByAptId(int id);

    }
}
