﻿using ApartmentRentals.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public class UserRepository:EntityRepository<AspNetUser,UserDTO>,IUserRepository
    {
         IContext _context;
         public UserRepository(IContext context)
             : base(context)
         {
             _context = context;
             _dbset = _context.Set<AspNetUser>();
         }

         public UserDTO GetById(string id)
         {
             return Mapper.Map<UserDTO>(_dbset.Where(t=>t.Id.Equals(id)).FirstOrDefault());
         }
    }
}
