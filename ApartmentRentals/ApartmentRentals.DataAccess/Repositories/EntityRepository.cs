﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.Repositories
{
    public abstract class EntityRepository<T, TDTO> : IEntityRepository<T, TDTO>
        where T : class
        where TDTO : class
    {
        protected IContext _context;
        protected IDbSet<T> _dbset;

        public EntityRepository(IContext context)
        {
            _context = context;
            _dbset = _context.Set<T>();
        }


        public virtual void Create(TDTO entityDTO)
        {
            if (entityDTO == null)
            {
                throw new ArgumentNullException("entity");
            }
            var entity = Mapper.Map<T>(entityDTO);

            _context.Entry(entity).State = EntityState.Added;
            _dbset.Add(entity);
            _context.SaveChanges();
        }


        public virtual void Update(TDTO entityDTO)
        {
            if (entityDTO == null)
            {
                throw new ArgumentNullException("entity");
            }
            _context.SaveChanges();
        }

        public virtual void Delete(TDTO entityDTO)
        {
            if (entityDTO == null)
            {
                throw new ArgumentNullException("entity");
            }
            var entity = Mapper.Map<T>(entityDTO);
            _dbset.Remove(entity);
            _context.SaveChanges();
        }

        public virtual IEnumerable<TDTO> GetAll()
        {

            return Mapper.Map<IEnumerable<T>, IEnumerable<TDTO>>(_dbset.AsEnumerable<T>());
        }

    }
}
