﻿using ApartmentRentals.DataAccess.Repositories;
using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.UnitsOfWork
{
    public class UserUnitOfWork:IUserUnitOfWork
    {
        IUserRepository _userRepository;
        IMessageRepository _messageRepository;
        IOfferRepository _offerRepository;
        IApartmentRepository _apartmentRepository;
        IContext _context;

        public UserUnitOfWork(IContext context, IUserRepository userRepo, IMessageRepository messageRepo, IOfferRepository offerRepo,IApartmentRepository apartmentRepo)
        {
            _userRepository = userRepo;
            _messageRepository = messageRepo;
            _offerRepository = offerRepo;
            _apartmentRepository = apartmentRepo;
            _context = context;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }



        #region IDisposable Members
        public void Dispose()
        {
            _userRepository = null;
            _messageRepository = null;
            _offerRepository = null;
            _apartmentRepository = null;
            _context = null;
        }
        #endregion


        public void Create(UserDTO entity)
        {
            _userRepository.Create(entity);
            this.SaveChanges();
        }

        public void Delete(UserDTO entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _userRepository.GetAll();
        }

        public void Update(UserDTO entity)
        {
            throw new NotImplementedException();
        }

        public UserDTO GetById(string id)
        {
            var apt= _userRepository.GetById(id);
            return apt;
        }

        public IEnumerable<MessageDTO> GetAllMessages(string sender, string receiver)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<UserDTO> GetAllConversations(string userId)
        {
            var list= _messageRepository.GetAllConversations(userId);
            var returnList = new List<UserDTO>();
            foreach(string item in list)
            {
                returnList.Add(_userRepository.GetById(item));
            }
            return returnList;

        }
        public void AddMessage(MessageDTO message)
        {
            _messageRepository.Create(message);
        }
        public IEnumerable<MessageDTO> GetConversation(string Id1,string Id2)
        {
            return _messageRepository.GetConversation(Id1, Id2);
        }

        public IEnumerable<OfferDTO> GetAllOffers(string Id)
        {
            var list=_offerRepository.GetAllOffers(Id);
            foreach(var item in list)
            {
                item.Apartment = _apartmentRepository.GetById(item.Apartment_Id);
            }
            return list;
        }
    }
}
