﻿using ApartmentRentals.DataAccess.Repositories;
using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.DataAccess.UnitsOfWork
{
    public class ApartmentUnitOfWork : IApartmentUnitOfWork
    {
        IApartmentRepository _apartmentRepository;
        IAmenitiesRepository _amenitiesRepository;
        IImagesRepository _imagesRepository;
        IOfferRepository _offerRepository;
        IContext _context;

        public ApartmentUnitOfWork(IContext context, IApartmentRepository apartmentRepo, IAmenitiesRepository amenitiesRepo, IImagesRepository imagesRepo, IOfferRepository offerRepo)
        {
            _apartmentRepository = apartmentRepo;
            _amenitiesRepository = amenitiesRepo;
            _imagesRepository = imagesRepo;
            _offerRepository = offerRepo;
            _context = context;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }



        #region IDisposable Members
        public void Dispose()
        {
            _apartmentRepository = null;
            _amenitiesRepository = null;
            _imagesRepository = null;
            _context = null;
        }
        #endregion


        public void Create(ApartmentDTO entity)
        {
            var amenities = new List<AmenitiesDTO>();
            foreach (var item in entity.Amenities1)
            {
                amenities.Add(_amenitiesRepository.GetById(item));
            }
            entity.Amenities = amenities;
            _apartmentRepository.Create(entity);
            _imagesRepository.Add(entity.ImagesUrl, entity.Id);
            this.SaveChanges();
        }

        public void Delete(ApartmentDTO entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ApartmentDTO> GetAll()
        {
            return _apartmentRepository.GetAll();
        }

        public void Update(ApartmentDTO entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AmenitiesDTO> GetAllAmenities()
        {
            return _amenitiesRepository.GetAll();
        }
        public ApartmentDTO GetById(int id)
        {
            var apt = _apartmentRepository.GetById(id);
            apt.Images = _imagesRepository.GetByAptId(id);
            return apt;
        }
        public void MakeOffer(OfferDTO offer)
        {
            _offerRepository.Create(offer);
        }


        public void AcceptOffer(OfferDTO offer)
        {
            var o = _offerRepository.GetById(offer.Id);

            o.Status_Id = "Accepted";
            o.Response = offer.Response;
            if (o.Response == null)
                o.Response = "";
            _offerRepository.Update(o);
            var list = _offerRepository.GetByAptId(o.Apartment_Id);
            foreach (var item in list)
            {
                if (item.Id != o.Id)
                {
                    item.Response = "Other offer accepted";
                    RefuseOffer(item);
                }
            }
            var apt = _apartmentRepository.GetById(o.Apartment_Id);
            apt.Status_Id = 2;
            _apartmentRepository.Update(apt);
            this.SaveChanges();
        }

        public void RefuseOffer(OfferDTO offer)
        {
            var o = _offerRepository.GetById(offer.Id);
            o.Status_Id = "Refused";
            o.Response = offer.Response;
            if (o.Response == null)
                o.Response = "";
            _offerRepository.Update(o);
            this.SaveChanges();
        }
    }
}
