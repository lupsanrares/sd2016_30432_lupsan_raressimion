﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ApartmentRentals.BL;

namespace ApartmentRentals.Controllers
{
    public class UserController : Controller
    {
        IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Authorize(Roles = "Owner, Tenant")]
        [HttpPost]
        public ActionResult Message(MessageDTO message)
        {
            try
            {
                message.Date = DateTime.Now;
                _userService.AddMessage(message);
                return RedirectToAction("Index","Apartments");
            }
            catch
            {
                return View();
            }
        }
         [Authorize(Roles = "Owner, Tenant")]
        public ActionResult Messages()
        { 
            var list=_userService.GetAllConversations(User.Identity.GetUserId());
             return View(list);
        }
         [Authorize(Roles = "Owner, Tenant")]
        public ActionResult Offers()
        {
            var list = _userService.GetAllOffers(User.Identity.GetUserId());
            return View(list);
        }
         [Authorize(Roles = "Owner, Tenant")]
        [HttpPost]
        public ActionResult Conversation(MessageDTO message)
        {
            try
            {
                return View(_userService.GetConversation(message.Sender_Id,message.Receiver_Id).OrderByDescending(d=>d.Date));
            }
            catch
            {
                return View();
            }
        }

    }
}
