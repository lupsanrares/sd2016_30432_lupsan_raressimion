﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApartmentRentals.Models
{
    public class ApartmentViewModel
    {
        public int Id { get; set; }
        public int Type_Id { get; set; }
        [Required]
        [Range(0,int.MaxValue)]
        public int Bathroom_Count { get; set; }
        [Required]
        [Range(0, int.MaxValue)]
        public int Bedroom_Count { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }
        [Required]
        public string Description { get; set; }
        public int Status_Id { get; set; }
        public string Owner_Id { get; set; }
        public string Owner_Name { get; set; }
        public string Tenant_Id { get; set; }
        [Required]
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
        public IEnumerable<ImageDTO> Images { get; set; }
        public IEnumerable<FileContentResult> Images2 { get; set; }
        public IEnumerable<AmenitiesDTO> Amenities { get; set; }
        public IEnumerable<int> Amenities1 { get; set; }
        public Enums.AptType Type { get; set; }
        public Enums.AptStatus Status { get; set; }
    }
}