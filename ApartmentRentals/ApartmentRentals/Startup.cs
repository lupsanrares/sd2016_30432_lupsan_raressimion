﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ApartmentRentals.Startup))]
namespace ApartmentRentals
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
