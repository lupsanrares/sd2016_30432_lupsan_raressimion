﻿using ApartmentRentals.DataAccess;
using ApartmentRentals.Model;
using ApartmentRentals.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentRentals.App_Start
{
    public static class InitializeAutoMapper
    {
        public static void Initialize()
        {
            Mapper.CreateMap<ApartmentViewModel, ApartmentDTO>();
            Mapper.CreateMap<ApartmentDTO, ApartmentViewModel>().ForMember(dest=>dest.Status,opts=>opts.MapFrom(src=>src.Status_Id))
                .ForMember(dest => dest.Type, opts => opts.MapFrom(src => src.Type_Id));
            Mapper.CreateMap<Apartment, ApartmentDTO>().ForMember(dest=>dest.Amenities,opts=>opts.MapFrom(src=>src.Facilities));
            Mapper.CreateMap<ApartmentDTO, Apartment>().ForMember(dest => dest.Facilities, opts => opts.MapFrom(src => src.Amenities)); ;
            Mapper.CreateMap<Facility, AmenitiesDTO>();
            Mapper.CreateMap<AmenitiesDTO, Facility>();

            Mapper.CreateMap<Image, ImageDTO>();
            Mapper.CreateMap<ImageDTO, Image>();
            Mapper.CreateMap<Message, MessageDTO>();
            Mapper.CreateMap<MessageDTO, Message>();
            Mapper.CreateMap<AspNetUser, UserDTO>();
            Mapper.CreateMap<UserDTO, AspNetUser>();
            Mapper.CreateMap<Offer, OfferDTO>();
            Mapper.CreateMap<OfferDTO, Offer>();
        }
    }
}