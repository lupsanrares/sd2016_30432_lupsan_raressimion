﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.Model
{
    public class AmenitiesDTO
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public bool Selected { get; set; }
    }
}
