﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.Model
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string PNC { get; set; }
        public string ICN { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}
