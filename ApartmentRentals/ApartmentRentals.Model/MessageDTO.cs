﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.Model
{
    public class MessageDTO
    {
        public int Id { get; set; }
        public string Sender_Id { get; set; }
        public string Receiver_Id { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime Date { get; set; }

        public UserDTO AspNetUser { get; set; }
        public UserDTO AspNetUser1 { get; set; }
    }
}
