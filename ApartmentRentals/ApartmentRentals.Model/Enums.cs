﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.Model
{
    public class Enums
    {
        public enum AptType
        {
            Detached=1,SemiDetached=2
        }
        public enum AptStatus
        {
            Available=1,Booked=2
        }
        public enum AccountType
        {
            Owner=1, Tenant=2
        }
    }
}
