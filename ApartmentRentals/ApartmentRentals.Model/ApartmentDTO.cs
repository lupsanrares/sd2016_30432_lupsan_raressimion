﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.Model
{
    public class ApartmentDTO
    {
        public int Id { get; set; }
        public int Type_Id { get; set; }
        public int Bathroom_Count { get; set; }
        public int Bedroom_Count { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public int Status_Id { get; set; }
        public string Owner_Id { get; set; }
        public string Owner_Name { get; set; }
        public string Tenant_Id { get; set; }
        public IEnumerable<ImageDTO> Images { get; set; }
        public List<string> ImagesUrl { get; set; }
        public Enums.AptType Type { get; set; }
        public Enums.AptStatus Status { get; set; }
        public IEnumerable<AmenitiesDTO> Amenities { get; set; }
        public IEnumerable<int> Amenities1 { get; set; }

    }
}
