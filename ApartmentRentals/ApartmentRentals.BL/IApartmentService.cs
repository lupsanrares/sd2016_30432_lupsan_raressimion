﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.BL
{
    public interface IApartmentService:IService<ApartmentDTO>
    {
        IEnumerable<AmenitiesDTO> GetAllAmenities();
        ApartmentDTO GetById(int id);
        void MakeOffer(OfferDTO offer);
        void AcceptOffer(OfferDTO offer);
        void RefuseOffer(OfferDTO offer);
    }
}
