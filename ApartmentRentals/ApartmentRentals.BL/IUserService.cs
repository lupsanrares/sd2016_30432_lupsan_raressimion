﻿using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.BL
{
    public interface IUserService:IService<UserDTO>
    {
        void AddMessage(MessageDTO message);
        IEnumerable<UserDTO> GetAllConversations(string Id);
        IEnumerable<MessageDTO> GetConversation(string Id1,string Id2);
        IEnumerable<OfferDTO> GetAllOffers(string Id);
    }
}
