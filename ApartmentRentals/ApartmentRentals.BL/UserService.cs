﻿using ApartmentRentals.DataAccess.UnitsOfWork;
using ApartmentRentals.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApartmentRentals.BL
{
    public class UserService:Service<UserDTO>,IUserService
    {
         IUserUnitOfWork _userUnitOfWork;
         public UserService(IUserUnitOfWork userUoW)
            : base(userUoW)
        {
            _userUnitOfWork = userUoW;

        }

         public IEnumerable<UserDTO> GetAllConversations(string Id)
         {
             return _userUnitOfWork.GetAllConversations(Id);
         }

         public void AddMessage(MessageDTO message)
         {
             _userUnitOfWork.AddMessage(message);
         }
        public IEnumerable<MessageDTO> GetConversation(string Id1,string Id2)
         {
             return _userUnitOfWork.GetConversation(Id1, Id2);
         }
        public IEnumerable<OfferDTO> GetAllOffers(string Id)
        {
            return _userUnitOfWork.GetAllOffers(Id);
        }
    }
}
